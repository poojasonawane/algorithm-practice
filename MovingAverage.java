/*
Space Complexity: O(n)
Time Complexity : O(1)
*/

public class MovingAverage {
    int[] q;
    double sum;
    int n, ins;
    /** Initialize your data structure here. */
    public MovingAverage(int size) {
        q = new int[size];
        sum = 0;
    }
    
    public double next(int val) {
        if(n < q.length)
            n++;
        sum -= q[ins];
        sum += val;
        q[ins] = val;
        ins = (++ins)%q.length;
        return sum/n;
    }
}

/**
 * Your MovingAverage object will be instantiated and called as such:
 * MovingAverage obj = new MovingAverage(size);
 * double param_1 = obj.next(val);
 */