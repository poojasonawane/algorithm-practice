/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

public class MaxProductSubArray {
    public int maxProduct(int A[]) {
    int r = A[0], n = A.length;
    for (int i = 1, imax = r, imin = r; i < n; i++) {
        if (A[i] < 0){
            int temp = imin;
            imin = imax;
            imax = temp;
        }
        imax = Math.max(A[i], imax * A[i]);
        imin = Math.min(A[i], imin * A[i]);
        r = Math.max(r, imax);
    }
    return r;
}
}