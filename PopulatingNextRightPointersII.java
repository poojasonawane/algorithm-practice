/*
Space complexity: O(1)
Time Complexity: O(n)
*/
public class PopulatingNextRightPointersII {
    public void connect(TreeLinkNode root) {
        TreeLinkNode curr = root, prev = null, head = null;
        while(curr != null){
            while(curr != null){
                if(curr.left != null){
                    if(prev != null)
                        prev.next = curr.left;
                    else
                        head = curr.left;
                    prev = curr.left;
                }
                if(curr.right != null){
                    if(prev != null)
                        prev.next = curr.right;
                    else
                        head = curr.right;
                    prev = curr.right;
                }
                curr = curr.next;
            }
            curr = head;
            head = null;
            prev = null;
        }
    }
}