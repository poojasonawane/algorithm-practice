/*
Space complexity: O(2^n)
Time Complexity: O(2^n)
it should be nd.
n is how many options you can choose from at each position.
d is how many positions.
*/
public class GenerateParenthesis {
    public List<String> generateParenthesis(int n) {
        List<String> res = new ArrayList<String>();
        backtrack(res, "", n, n);
        return res;
    }
    private void backtrack(List<String> res, String s, int open, int close){
        if(open == 0 && close == 0){
            res.add(s);
            return;
        }
        if(open > 0){
            backtrack(res, s + "(", open - 1, close);
        }
        if(close > open){
            backtrack(res, s + ")", open, close - 1);
        }
    }
}