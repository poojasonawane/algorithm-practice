/*
Space complexity: O(n)
Time Complexity: O(n)
*/
public class kthSmallestInBST {
    public int kthSmallest(TreeNode root, int k) {
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode curr = root;
        while(curr != null || !stack.isEmpty()){
            while(curr != null){
                stack.push(curr);
                curr = curr.left;
            }
            curr = stack.pop();
            if(--k == 0) break;
            curr = curr.right;
        }
        return curr.val;
    }
}