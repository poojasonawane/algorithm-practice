/*
Space complexity: O(log n) for balanced tree else O(n)
Time Complexity: O(n)
*/
/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
public class MeetingRoomsII {
    public int minMeetingRooms(Interval[] intervals) {
        if(intervals.length < 2)
            return intervals.length;
        int start[] = new int[intervals.length];
        int end[] = new int[intervals.length];
        for(int i = 0; i < intervals.length; i++){
            start[i] = intervals[i].start;
            end[i] = intervals[i].end;
        }
        Arrays.sort(start);
        Arrays.sort(end);
        int endItr = 0, rooms = 0;
        for(int i = 0; i < start.length; i++){
            if(start[i] < end[endItr])
                rooms++;
            else
                endItr++;
        }
        return rooms;
    }
}
/*
public int minMeetingRooms(Interval[] intervals) {
    if(intervals.length < 2)
        return intervals.length;
    Arrays.sort(intervals, (x, y) -> x.start - y.start);
    ArrayList<Integer> rooms = new ArrayList();
    rooms.add(intervals[0].end);
    for(int i = 1; i < intervals.length; i++){
        for(int r : rooms){
            if(r <= intervals[i].start){
                rooms.remove(new Integer(r));
                break;
            }
        }
        rooms.add(intervals[i].end);
    }
    return rooms.size();
}
*/