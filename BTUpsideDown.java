/*
Space complexity: O(1)
Time Complexity: O(log n)
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class BTUpsideDown {
    TreeNode res = null;
    public TreeNode upsideDownBinaryTree(TreeNode root) {
        TreeNode curr = root;
        TreeNode next = null, parent = null, sib = null;
        while(curr != null){
            next = curr.left;
            curr.left = sib;
            sib = curr.right;
            curr.right = parent;
            parent = curr;
            curr = next;
        }
        return parent;
    }
}