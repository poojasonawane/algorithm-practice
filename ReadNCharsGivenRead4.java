/*
Space Complexity: O(1)
Time Complexity : O(n)
*/
/* The read4 API is defined in the parent class Reader4.
      int read4(char[] buf); */

public class ReadNCharsGivenRead4 extends Reader4 {
    /**
     * @param buf Destination buffer
     * @param n   Maximum number of characters to read
     * @return    The number of characters read
     */
    public int read(char[] buf, int n) {
        char[] buf4 = new char[4];
        int nRead = 0, pos = 0;
        while (((nRead = read4(buf4)) > 0) && (pos < n)) { //can read && can write
            for (int i = 0; i < nRead && pos < n; i++) //can write
                buf[pos++] = buf4[i];
        }
        return pos;   
    }
}