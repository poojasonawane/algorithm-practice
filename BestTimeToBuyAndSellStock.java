/*
Space complexity: O(1)
Time Complexity: O(n)
*/
public class BestTimeToBuyAndSellStock {
    public int maxProfit(int[] prices) {
        int maxCur = 0, maxSoFar = 0;
        for(int i = 1; i < prices.length; i++) {
            maxCur = Math.max(0, maxCur + prices[i] - prices[i-1]);
            maxSoFar = Math.max(maxCur, maxSoFar);
        }
        return maxSoFar;
    }
}
/*
public int maxProfit(int[] prices) {
	int max = 0, min = 0;
	for(int j = 0; j < prices.length; j++){
		if(prices[j] > prices[min]){
			max = Math.max(max, prices[j] - prices[min]);
		}else{
			min = j;
		}
	}
	return max;
}
*/