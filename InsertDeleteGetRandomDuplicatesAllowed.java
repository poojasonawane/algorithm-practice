/*
Space complexity: O(n)
Time Complexity: O(1)
*/
public class RandomizedCollection {

    HashMap<Integer, ArrayList<Integer>> locs;
    ArrayList<Integer> nums;
    Random rand;
    
    /** Initialize your data structure here. */
    public RandomizedCollection() {
        locs = new HashMap();
        nums = new ArrayList();
        rand = new Random();
    }
    
    /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
    public boolean insert(int val) {
        ArrayList<Integer> list;
        if(locs.containsKey(val)){
            list = locs.get(val);
        }else{
            list = new ArrayList();
        }
        list.add(nums.size());
        locs.put(val, list);
        nums.add(val);
        return list.size() == 1;
    }
    
    /** Removes a value from the collection. Returns true if the collection contained the specified element. */
    public boolean remove(int val) {
        if(!locs.containsKey(val)){
            return false;
        }
        ArrayList<Integer> target = locs.get(val);
        int loc = target.get(target.size()-1);
        target.remove(target.size()-1);
        if(loc < nums.size()-1){
            int last = nums.get(nums.size()-1);
            ArrayList<Integer> lastList = locs.get(last);
            nums.set(loc, last);
            lastList.remove(new Integer(nums.size()-1));
            lastList.add(loc);
            locs.put(last, lastList);
        }
        nums.remove(nums.size()-1);
        if(target.size() == 0){
            locs.remove(val);
        }else{
            locs.put(val, target);
        }
        return true;
    }
    
    /** Get a random element from the collection. */
    public int getRandom() {
        return nums.get(rand.nextInt(nums.size()));
    }
}

/**
 * Your RandomizedCollection object will be instantiated and called as such:
 * RandomizedCollection obj = new RandomizedCollection();
 * boolean param_1 = obj.insert(val);
 * boolean param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */