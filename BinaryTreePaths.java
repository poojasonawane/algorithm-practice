/*
Space complexity: O(n)
Time Complexity: O(n)
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public List<String> binaryTreePaths(TreeNode root) {
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode prev=null, curr=root;
        List<String> result = new LinkedList<String>();
        StringBuilder path = new StringBuilder();
        while(curr!=null || !stack.isEmpty()){
            while(curr!=null){
                stack.push(curr);
                path.append(Integer.toString(curr.val)+"->");
                curr=curr.left;
            }
            curr=stack.peek();
            if(curr.left==null && curr.right==null){
                path.delete(path.length()-2, path.length());
                result.add(new String(path));
            }
            if(curr.right!=null && curr.right!=prev)
                curr=curr.right;
            else{
                prev=curr;
                stack.pop();
                curr=null;
                if(path.charAt(path.length()-1)=='>')
                    path.deleteCharAt(path.length()-1);
                int i = path.lastIndexOf(">");
                if(i != -1)
                    path.replace(i+1, path.length(), "");
            }
        }
        return result;
    }
}
/*
public List<String> binaryTreePaths(TreeNode root) {
    List<String> answer = new ArrayList<String>();
    if (root != null) searchBT(root, "", answer);
    return answer;
}
private void searchBT(TreeNode root, String path, List<String> answer) {
    if (root.left == null && root.right == null) answer.add(path + root.val);
    if (root.left != null) searchBT(root.left, path + root.val + "->", answer);
    if (root.right != null) searchBT(root.right, path + root.val + "->", answer);
}
*/