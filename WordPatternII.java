/*
Time Complexity: O(n ^ m)
*/
public class WordPatternII {
    public boolean wordPatternMatch(String pattern, String str) {
        return helper(pattern, str, new HashMap<Character, String>());
    }
    private boolean helper(String pattern, String str, HashMap<Character, String> map){
        if(pattern.length() == 0){
            return str.length() == 0;
        }
        Character key = pattern.charAt(0);
        if(map.containsKey(key)){
            String val = map.get(key);
            if(str.length() < val.length() || !str.substring(0, val.length()).equals(val))
                return false;
            if(helper(pattern.substring(1), str.substring(val.length()), map))
                return true;
        }else{
            for(int i = 1; i <= str.length(); i++){
                String val = str.substring(0, i);
                if(map.containsValue(val))
                    continue;
                map.put(key, val);
                if(helper(pattern.substring(1), str.substring(i), map))
                    return true;
                map.remove(key);
            }
        }
        return false;
    }
}

/*
public class Solution {
    public boolean wordPatternMatch(String pattern, String str) {
        return helper(pattern, str, new HashMap<Character, String>(), new HashSet<String>(), 0, 0);
    }
    private boolean helper(String pattern, String str, HashMap<Character, String> map, HashSet<String> set, int startS, int startP){
        if (startS == str.length() && startP == pattern.length()) 
            return true;
        if (startS == str.length() || startP == pattern.length()) 
            return false;
        Character key = pattern.charAt(startP);
        if(map.containsKey(key)){
            String val = map.get(key);
            if(!str.startsWith(val, startS))
                return false;
            return helper(pattern, str, map, set, startS + val.length(), startP + 1);
        }
        for(int i = startS; i < str.length(); i++){
            String val = str.substring(startS, i + 1);
            if(set.contains(val))
                continue;
            map.put(key, val);
            set.add(val);
            if(helper(pattern, str, map, set, i + 1, startP + 1))
                return true;
            map.remove(key);
            set.remove(val);
        }
        return false;
    }
}
*/