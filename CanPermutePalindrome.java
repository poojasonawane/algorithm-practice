/*
Space complexity: O(n)
Time Complexity: O(n)
*/
import java.util.*;

public class CanPermutePalindrome {
    public boolean canPermutePalindrome(String s) {
        Set set = new HashSet<Character>();
        for(int i = 0; i < s.length(); i++){
            if(!set.add(s.charAt(i)))
                set.remove(s.charAt(i));
        }
        return set.size() < 2;
    }
}

/*
can use int array of size 128 & store ascii

public boolean canPermutePalindrome(String s) {
    BitSet bs = new BitSet();
    for (byte b : s.getBytes())
        bs.flip(b);
    return bs.cardinality() < 2;
}
*/