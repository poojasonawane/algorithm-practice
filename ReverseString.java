/*
Space complexity: O(s)
Time Complexity: O(n)
*/
import java.util.*;

public class ReverseString {
    public String reverseString(String s) {
        StringBuilder rev = new StringBuilder();
        rev.setLength(s.length());
        for(int i = 0; i < s.length(); i++){
            rev.setCharAt(i,s.charAt(s.length()-i-1));
        }   
        return rev.toString();
    }
}
/*
public String reverseString(String s) {
    if(s == null || s.length() == 0)
        return "";
    char[] cs = s.toCharArray();
    int begin = 0, end = s.length() - 1;
    while(begin < end){
        char c = cs[begin];
        cs[begin] = cs[end];
        cs[end] = c;
        begin++;
        end--;
    }
    
    return new String(cs);
}
*/