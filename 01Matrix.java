/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

public class 01Matrix {
    public int[][] updateMatrix(int[][] matrix) {
        if(matrix.length == 0 || matrix[0].length == 0)
            return matrix;
        int[][] res = new int[matrix.length][matrix[0].length];
        int max = matrix.length * matrix[0].length;
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[i].length; j++){
                if(matrix[i][j] != 0){
                    int u = i == 0 ? max : res[i - 1][j];
                    int l = j == 0 ? max : res[i][j - 1];
                    res[i][j] = Math.min(u, l) + 1;
                }
            }
        }
        for(int i = matrix.length - 1; i >= 0; i--){
            for(int j = matrix[0].length - 1; j >= 0; j--){
                if(matrix[i][j] != 0){
                    int d = i == matrix.length - 1 ? max : res[i + 1][j];
                    int r = j == matrix[0].length - 1 ? max : res[i][j + 1];
                    res[i][j] = Math.min(res[i][j], Math.min(r, d) + 1);
                }
            }
        }
        return res;
    }
}