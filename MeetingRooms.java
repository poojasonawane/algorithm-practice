/*
Space complexity: O(1)
Time Complexity: O(n log n)
*/
import java.util.*;
/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
public class MeetingRooms {
    public boolean canAttendMeetings(Interval[] intervals) {
        //Arrays.sort(intervals, (x, y) -> x.start - y.start);
        Arrays.sort(intervals, new Comparator<Interval>(){
            public int compare(Interval x, Interval y){
                return x.start - y.start;
            }
        });
        for(int i = 1; i < intervals.length; i++){
            if(intervals[i-1].end > intervals[i].start)
                return false;
        }   
        return true;
    }
}