/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class BoundaryOfBinaryTree {
    List<Integer> res = new ArrayList<Integer>();
    public List<Integer> boundaryOfBinaryTree(TreeNode root) {
        if(root == null)
            return res;
        res.add(root.val);
        left(root.left);
        leaves(root.left);
        leaves(root.right);
        right(root.right);
        return res;   
    }
    private void left(TreeNode root){
        if(root == null || (root.left == null && root.right == null))
            return;
        res.add(root.val);
        if(root.left == null)
            left(root.right);
        else
            left(root.left);
    } 
    private void right(TreeNode root){
        if(root == null || (root.left == null && root.right == null))
            return;
        if(root.right == null)
            right(root.left);
        else
            right(root.right);
        res.add(root.val);
    } 
    private void leaves(TreeNode root){
        if(root == null)
            return;
        if(root.left == null && root.right == null)
            res.add(root.val);
        leaves(root.left);
        leaves(root.right);
    }
}