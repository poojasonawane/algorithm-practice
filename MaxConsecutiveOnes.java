/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

public class MaxConsecutiveOnes {
    public int findMaxConsecutiveOnes(int[] nums) {
         int max = 0, maxSoFar = 0;
         for(int num : nums){
             maxSoFar = Math.max(maxSoFar, max = num == 1 ? ++max : 0);
         }  
         return maxSoFar;
    }
}