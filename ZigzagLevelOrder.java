/*
Space complexity: O(w)
Time Complexity: O(n)
*/
public class ZigzagLevelOrder {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        if(root == null) return result;
        Stack<TreeNode> currLevel = new Stack<TreeNode>();
        Stack<TreeNode> nextLevel = new Stack<TreeNode>();
        currLevel.push(root);
        while(!currLevel.isEmpty() || !nextLevel.isEmpty()){
            List subList = new ArrayList<Integer>();
            if(!currLevel.isEmpty()){
                while(!currLevel.isEmpty()){
                    TreeNode curr = currLevel.pop();
                    subList.add(curr.val);
                    if(curr.left != null) 
                        nextLevel.push(curr.left);
                    if(curr.right != null)
                        nextLevel.push(curr.right);
                }
                result.add(subList);
                continue;
            }
            if(!nextLevel.isEmpty()){
                while(!nextLevel.isEmpty()){
                    TreeNode curr = nextLevel.pop();
                    subList.add(curr.val);
                    if(curr.right != null)
                        currLevel.push(curr.right);
                    if(curr.left != null)
                        currLevel.push(curr.left);
                }
                result.add(subList);
            }
        }
        return result;
    }
}