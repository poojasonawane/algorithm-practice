/*
Space complexity: O(n)
Time Complexity: O(n)
*/
public class BinaryTreefromInorderandPostorder {
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        return build(inorder, 0 , inorder.length-1, postorder, 0, postorder.length-1);
    }
    public TreeNode build(int[] inorder, int inStart, int inEnd, int[] postorder, int postStart, int postEnd){
        if(inStart > inEnd || postStart > postEnd)
            return null;
        TreeNode curr = new TreeNode(postorder[postEnd]);
        int offset;
        for(offset = inStart; offset < inEnd; offset++){
            if(inorder[offset] == curr.val)
                break;
        }
        curr.left = build(inorder, inStart, offset-1, postorder, postStart, postStart+offset-inStart-1);
        curr.right = build(inorder, offset+1, inEnd, postorder, postStart+offset-inStart, postEnd-1);
        return curr;
    }
}