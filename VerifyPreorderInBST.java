/*
Space complexity: O(1)
Time Complexity: O(n^2)
*/
public class VerifyPreorderInBST {
    public boolean verifyPreorder(int[] preorder) {
        for(int i = 0; i < preorder.length-1; i++){
            int j = i+1;
            while(j < preorder.length && preorder[j] < preorder[i])
                j++;
            while(j < preorder.length && preorder[j] > preorder[i])
                j++;
            if(j != preorder.length)
                return false;
        }
        return true;
    }
}
/*
Changing array
Space complexity: O(1)
Time Complexity: O(n)
public boolean verifyPreorder(int[] preorder) {
    int low = Integer.MIN_VALUE, i = -1;
    for (int p : preorder) {
        if (p < low)
            return false;
        while (i >= 0 && p > preorder[i])
            low = preorder[i--];
        preorder[++i] = p;
    }
    return true;
}

Space complexity: O(n)
Time Complexity: O(n)
public boolean verifyPreorder(int[] preorder) {
    int low = Integer.MIN_VALUE;
    Stack<Integer> path = new Stack();
    for (int p : preorder) {
        if (p < low)
            return false;
        while (!path.empty() && p > path.peek())
            low = path.pop();
        path.push(p);
    }
    return true;
}
*/