/*
Space Complexity: O(1)
Time Complexity : O(n)
*/

public class ValidWordAbbreviation {
    public boolean validWordAbbreviation(String word, String abbr) {
        return word.matches(abbr.replaceAll("[1-9]\\d*", ".{$0}"));
    }
}
/*
public boolean validWordAbbreviation(String word, String abbr) {
        int l = word.length(), i = 0, j = 0;
        while(i < word.length() && j < abbr.length()){
            if(word.charAt(i) == abbr.charAt(j)){
                i++;
                j++;
            }else if(Character.isDigit(abbr.charAt(j)) && abbr.charAt(j) != '0'){
                int k = j;
                while(j < abbr.length() && Character.isDigit(abbr.charAt(j))) j++;
                i += Integer.parseInt(abbr.substring(k,j));
            }else
                break;
        }
        return i == word.length() && j == abbr.length();
    }
*/