/*
h-index is defined as the number of papers with reference greater than the number.
Then we iterate from the back to the front of the buckets, whenever the total count exceeds the index of the bucket, meaning that we have the index number of papers that have reference greater than or equal to the index. Which will be our h-index result. The reason to scan from the end of the array is that we are looking for the greatest h-index.

Space complexity: O(n)
Time Complexity: O(n)
*/
import java.util.*;

public class HIndex {
    public int hIndex(int[] citations) {
        int n = citations.length;
        int[] papers = new int[n + 1];
        for(int c : citations)
            papers[Math.min(n, c)]++;
        int h = n;
        for(int s = papers[n]; h > s; s += papers[h])
            h--;
        return h;
    }
}