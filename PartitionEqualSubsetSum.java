/*
Space complexity: O(sum)
Time Complexity: O(n * sum)
*/
public class PartitionEqualSubsetSum {
    public boolean canPartition(int[] nums) {
        int sum = 0;
        for(int n : nums){
            sum += n;
        }
        if(sum % 2 == 1){
            return false;
        }
        sum /= 2;
        boolean[] sack = new boolean[sum+1];
        sack[0] = true;
        for(int n : nums){
            for(int i = sum; i >= n && !sack[sum]; i--){
                sack[i] = sack[i] || sack[i - n];
            }
        }
        return sack[sum];
    }
}