/*
Time Complexity : O(n)
*/
class Node{
    boolean isEnd;
    HashMap<Character, Node> next;
    
    public Node(){
        isEnd = false;
        next = new HashMap();
    }
}
public class WordDictionary {

    Node root;
    /** Initialize your data structure here. */
    public WordDictionary() {
        root = new Node();
    }
    
    /** Adds a word into the data structure. */
    public void addWord(String word) {
        Node curr = root;
        for(Character ch : word.toCharArray()){
            if(!curr.next.containsKey(ch))
                curr.next.put(ch, new Node());
            curr = curr.next.get(ch);
        }
        curr.isEnd = true;
    }
    
    /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
    public boolean search(String word) {
        return match(word, root, 0);
    }
    
    private boolean match(String word, Node start, int idx){
        if(start == null)
            return false;
        if(idx == word.length())
            return start.isEnd;
        Character ch = word.charAt(idx);
        if(ch != '.'){
            return match(word, start.next.get(ch), idx+1);
        }else{
            for(Character key : start.next.keySet()){
                if(match(word, start.next.get(key), idx+1))
                    return true;
            }
        }
        return false;
    }
}

/**
 * Your WordDictionary object will be instantiated and called as such:
 * WordDictionary obj = new WordDictionary();
 * obj.addWord(word);
 * boolean param_2 = obj.search(word);
 */