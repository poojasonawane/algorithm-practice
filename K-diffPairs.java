/*
Space complexity: O(n)
Time Complexity: O(n)
*/
import java.util.*;

public class K-diffPairs {
    public int findPairs(int[] nums, int k) {
        if(nums == null || nums.length == 0 || k < 0)
            return 0;
        int count = 0;
        HashMap<Integer, Integer> map = new HashMap<>();
        for(int num : nums){
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        for(Map.Entry<Integer, Integer> entry : map.entrySet()){
            if(k == 0){
                if(entry.getValue() > 1)
                    count++;
            }else{
                if(map.containsKey(entry.getKey() + k))
                    count++;
            }
        }
        return count;
    }
}