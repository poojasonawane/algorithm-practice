/*
Space complexity: O(n)
Time Complexity: O(n)
*/
import java.util.*;

public class ZigZagConverssion {
    public String convert(String s, int numRows) {
        if(numRows == 1)
            return s;
        StringBuilder[] rows = new StringBuilder[numRows];
        StringBuilder res = new StringBuilder();
        int i, j;
        for(i=0; i<rows.length; i++) {
            rows[i]=new StringBuilder();
        }
        j = 0;
        while(j < s.length()){
            for(i = 0; i < numRows && j < s.length(); i++){
                rows[i].append(s.charAt(j++));
            }
            for(i = numRows-2; i > 0 && j < s.length(); i--){
                rows[i].append(s.charAt(j++));
            }
        }
        for(i = 0; i < numRows; i++){
            for(j = 0; j < rows[i].length(); j++){
                res.append(rows[i].charAt(j));
            }
        }
        return res.toString();
    }
}
/*
Space: O(1)
public String convert(String s, int nRows) {
        if (nRows == 1)
            return s;
        StringBuilder strBuilder = new StringBuilder();
        int borderRowStep = 2 * nRows - 2;
        for (int i = 0; i < nRows; i++) {
            if (i == 0 || i == nRows - 1) {
                for (int j = i; j < s.length(); j = j + borderRowStep) {
                    strBuilder.append(s.charAt(j));
                }
            } else {
                int j = i;
                boolean flag = true;
                int insideRowLargeStep = 2 * (nRows - 1 - i);
                int insideRowSmallStep = borderRowStep - insideRowLargeStep;
                while (j < s.length()) {
                   strBuilder.append(s.charAt(j));
                    if (flag)
                        j = j + insideRowLargeStep;
                    else
                        j = j + insideRowSmallStep;
                    flag = !flag;
                }
            }
        }
        return strBuilder.toString();
    
}

*/