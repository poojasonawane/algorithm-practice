/*
Space complexity: O(p)
Time Complexity: O(s)
*/
import java.util.*;

public class MinWindowSubstring {
    public String minWindow(String s, String t) {
        int head = 0, begin = 0, end = 0, len = Integer.MAX_VALUE;
        if(t.length() > s.length())
            return "";
        HashMap<Character, Integer> map = new HashMap();
        for(Character ch : t.toCharArray()){
            map.put(ch, map.getOrDefault(ch, 0)+1);
        }
        int counter = map.size();
        while(end < s.length()){
            char ch = s.charAt(end);
            if(map.containsKey(ch)){
                map.put(ch, map.get(ch) - 1);
                if(map.get(ch) == 0)
                    counter--;
            }
            end++;
            while(counter == 0){
                ch = s.charAt(begin);
                if(map.containsKey(ch)){
                    map.put(ch, map.get(ch) + 1);
                    if(map.get(ch) > 0)
                        counter++;
                    if(end - begin < len){
                        len = end - begin;
                        head = begin;
                    }
                }
                begin++;
            }
        }
        return len == Integer.MAX_VALUE ? "" : s.substring(head, head+len);
    }
}