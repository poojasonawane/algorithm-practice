/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

public class RomanToInt {
    public int romanToInt(String s) {
        int sum = 0;
        for(int i = s.length()-1; i >= 0; i--){
            switch(s.charAt(i)){
                case 'M' :
                    sum += 1000;
                    break;
                case 'D' :
                    sum += 500;
                    break;
                case 'C' :
                    sum += 100 * (sum >= 500 ? -1 : 1);
                    break;
                case 'L' : 
                    sum += 50;
                    break;
                case 'X' :
                    sum += 10 * (sum >= 50 ? -1 : 1);
                    break;
                case 'V' :
                    sum += 5;
                    break;
                case 'I' :
                    sum += 1 * (sum >= 5 ? -1 : 1);
                    break;
            }
        }
        return sum;
    }
}
/*
Approach 1:
store vals of romans in another array. or in hash table
scan array n add if curr >= next else subtract
*/