/*
Space complexity: O(2 ^ n)
Time Complexity: O(2 ^ n)
*/
public class Permutations {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> res = new ArrayList<List<Integer>>();
        backtrack(nums, res, new ArrayList<Integer>());
        return res;
    }
    private void backtrack(int[] nums, List<List<Integer>> res, List<Integer> sub){
        if(sub.size() == nums.length){
            res.add(new ArrayList<Integer>(sub));
        }else{
            for(int i = 0; i < nums.length; i++){
                if(sub.contains(nums[i]))continue;
                sub.add(nums[i]);
                backtrack(nums, res, sub);
                sub.remove(sub.size()-1);
            }
        }
    }
}