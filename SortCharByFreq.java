/*
Space complexity: O(n)
Time Complexity: O(n)
*/
import java.util.*;

public class SortCharByFreq {
    public String frequencySort(String s) {
        char[] charArray = s.toCharArray();
        HashMap<Character, Integer> map = new HashMap();
        int max = 0;
        for(char c : charArray){
            map.put(c, map.getOrDefault(c, 0) + 1);
            max = Math.max(max, map.get(c));
        }
        List<Character>[] counts = new List[max+1];
        for(Character c : map.keySet()){
            int freq = map.get(c);
            if(counts[freq] == null){
                counts[freq] = new ArrayList();
            }
            counts[freq].add(c);
        }
        StringBuilder res = new StringBuilder();
        for(int i = max; i >= 0; i--){
            if(counts[i] != null){
                for(Character c : counts[i]){
                    for(int j = 0; j < i; j++){
                        res.append(c);
                    }
                }
            }
        }
        return res.toString();
    }
}