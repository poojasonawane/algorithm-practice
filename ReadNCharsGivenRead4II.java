/*
Space Complexity: O(1)
Time Complexity : O(n)
*/

public class ReadNCharsGivenRead4II extends Reader4 {
    /**
     * @param buf Destination buffer
     * @param n   Maximum number of characters to read
     * @return    The number of characters read
     */
    private int buffPtr = 0;
    private int buffCnt = 0;
    private char[] buff = new char[4]; 
    public int read(char[] buf, int n) {
        int ptr = 0;
        while(ptr < n){
            if(buffPtr == 0){
                buffCnt = read4(buff);
            }
            if(buffCnt == 0)
                break;
            while(ptr < n && buffPtr < buffCnt)
                buf[ptr++] = buff[buffPtr++];
            if(buffPtr == buffCnt)
                buffPtr = 0;
        }
        return ptr;
    }
}

/*
public int read(char[] buf, int n) {
        int i = 0;
        while (i < n && (i4 < n4 || (i4 = 0) < (n4 = read4(buf4))))
            buf[i++] = buf4[i4++];
        return i;
    }
    char[] buf4 = new char[4];
    int i4 = 0, n4 = 0;
*/