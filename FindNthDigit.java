/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

public class FindNthDigit {
    public int findNthDigit(int n) {
        if(n < 1) return 0;
        if(n < 10) return n;
        int counter = 1;  //stores the level number
        int base = 0;      //stores the biggest number from previous level
        while(n > (9 * Math.pow(10,counter -1) * (counter))){
            base += 9 * Math.pow(10,counter -1);
            n -= (9 * Math.pow(10,counter -1) * (counter));
            counter++;
        }
        //target is the actual number that has nth digit
        int target = base + ((n + counter - 1) / counter);  //to get the ceiling of n / counter
        int offset = n % counter;
        offset = (offset == 0) ? 0:counter - offset;
        for(int i = 0; i < offset; i++){
            target = target / 10;
        }
        return target % 10;
    }
}