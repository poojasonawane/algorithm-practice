/*
Space complexity: O(n)
Time Complexity: O(n)
*/
public class WordPAttern {
    public boolean wordPattern(String pattern, String str) {
        Map map = new HashMap();
        String[] tokens = str.split(" ");
        if(tokens.length != pattern.length()){
            return false;
        }
        for(Integer i = 0; i < pattern.length(); i++){
            if(map.put(pattern.charAt(i), i) != map.put(tokens[i], i)){
                return false;
            }
        }
        return true;
    }
}

/*
public class Solution {
    public boolean wordPattern(String pattern, String str) {
        HashMap<String, Character> map = new HashMap<String, Character>();
        String[] tokens = str.split(" ");
        if(tokens.length != pattern.length()){
            return false;
        }
        for(int i = 0; i < pattern.length(); i++){
            Character val = map.getOrDefault(tokens[i], null);
            if(val == null && !map.containsValue(pattern.charAt(i))){
                map.put(tokens[i], pattern.charAt(i));
            }else if(val == null || val != pattern.charAt(i)){
                return false;
            }
        }
        return true;
    }
}
*/