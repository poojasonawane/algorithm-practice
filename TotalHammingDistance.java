/*
Space Complexity: O(l)
Time Complexity : O(n*log s) s is size of int
*/

public class TotalHammingDistance {
    public int totalHammingDistance(int[] nums) {
        int res = 0;
        int[] cnt = new int[32];
        for(int num : nums){
            int c = 31;
            while(num > 0){
                cnt[c--] += num & 1;
                num >>= 1;
            }
        }
        for(int k : cnt)
            res += k * (nums.length - k);
        return res;
    }
}