/*
Space complexity: O(2 ^ n)
Time Complexity: O(2 ^ n)
*/
public class Combinations {
    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> res = new ArrayList<List<Integer>>();
        helper(res, n, k, new ArrayList<Integer>(), 1);
        return res;
    }
    private void helper(List<List<Integer>> res, int n, int k, List<Integer> sub, int pos){
        if(pos > n || k == 0){
            if(k == 0)
                res.add(new ArrayList(sub));
            return;
        }
        for(int i = pos; i <= n; i++){
            sub.add(i);
            helper(res, n, k-1, sub, i+1);
            sub.remove(sub.size()-1);
        }
    }
}