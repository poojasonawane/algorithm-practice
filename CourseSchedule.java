/*
Space complexity: O(n ^ 2)
Time Complexity: O(n + e)
*/
public class CourseSchedule {
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        int[][] matrix = new int[numCourses][numCourses];
        int[] indeg = new int[numCourses];
        for(int i = 0; i < prerequisites.length; i++){
            int curr = prerequisites[i][0];
            int pre = prerequisites[i][1];
            matrix[pre][curr] = 1;
            indeg[curr]++;
        }
        Queue<Integer> q = new LinkedList();
        for(int i = 0; i < indeg.length; i++){
            if(indeg[i] == 0){
                q.offer(i);
            }
        }
        int count = 0;
        while(!q.isEmpty()){
            int done = q.poll();
            count++;
            for(int c = 0; c < matrix[done].length; c++){
                if(matrix[done][c] > 0){
                    if(--indeg[c] == 0){
                        q.offer(c);
                    }
                }
            }
        }
        return count == numCourses;
    }