/*
Space complexity: O(1)
Time Complexity: O(log n)
*/
public class FindPeakElement {
    public int findPeakElement(int[] num) {
    
    if (num.length <= 1) return 0;
    int mid = 0, l = 0, h = num.length- 1;
    
    while (l < h) {
        mid = (l + h) / 2;
        if (num[mid] > num[mid + 1])
            h = mid;
        else 
            l = mid + 1;
    }
    
    return h; //or l
    }
}
/*
Time: O(n)
	for(int i = 1; i < num.length; i ++)
        {
            if(num[i] < num[i-1])
                return i-1;
        }
        return num.length-1;
    }
*/