/*
Space complexity: O(n)
Time Complexity: O(mn)
*/
public class LonelyPixel {
    public int findLonelyPixel(char[][] picture) {
        int count = 0;
        int[] cols = new int[picture[0].length];
        for(int i = 0; i < picture.length; i++){
            for(int j = 0; j < picture[0].length; j++){
                if(picture[i][j] == 'B'){
                    cols[j]++;
                }
            }
        }
        for(int i = 0; i < picture.length; i++){
            int col = 0, pos = 0;
            for(int j = 0; j < picture[0].length; j++){
                if(picture[i][j] == 'B'){
                    col++;
                    pos = j;
                }
            }
            if(col == 1 && cols[pos] == 1)
                count++;
        }
        return count;
    }
    
}