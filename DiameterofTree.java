/*
Space complexity: O(h)
Time Complexity: O(n)
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    private int max = 0;
    public int diameterOfBinaryTree(TreeNode root) {
        pathMax(root);
        return max;
    }
    private int pathMax(TreeNode root){
        if(root == null)
            return 0;
        int lh = pathMax(root.left);
        int rh = pathMax(root.right);
        max = Math.max(lh + rh, max);
        return Math.max(lh, rh) + 1;
    }
}