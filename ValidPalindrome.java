/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

public class ValidPalindrome {
    public boolean isPalindrome(String s) {
        for(int i = 0, j = s.length()-1; i < j; i++, j--){
            while(i < s.length() && !Character.isLetterOrDigit(s.charAt(i)))
                i++;
            while(j >= 0 && !Character.isLetterOrDigit(s.charAt(j)))
                j--;
            if(i < j && Character.toLowerCase(s.charAt(i)) != Character.toLowerCase(s.charAt(j)))
                return false;
        }
        return true;
    }
}
/*
public boolean isPalindrome(String s){
    s=s.toLowerCase().replaceAll("[^a-z0-9]", "");
    return new StringBuilder(s).reverse().toString().equals(s);
}
*/