/*
Space Complexity: O(1)
Time Complexity : O(n)
*/

public class SortColors {
    public void sortColors(int[] nums) {
        int f = 0, l = nums.length - 1;
        for (int i=0; i<=l; i++) {
                while (nums[i]==2 && i < l) swap(nums, i, l--);
                while (nums[i]==0 && i > f) swap(nums, i, f++);
        }
    }
    private void swap(int[] nums, int x, int y){
        int temp = nums[x];
        nums[x] = nums[y];
        nums[y] = temp;
    }
}