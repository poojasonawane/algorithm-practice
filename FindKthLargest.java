/*
Space complexity: O(1)
Time Complexity: O(n)
Discard half each time: n+(n/2)+(n/4)..1 = n + (n-1) = O(2n-1) = O(n), because n/2+n/4+n/8+..1=n-1
*/
public class FindKthLargest {
    public int findKthLargest(int[] nums, int k) {
        if(nums.length == 0)
            return 0;
        return findKthLargest(nums, 0, nums.length - 1, nums.length - k);
    }
    private int findKthLargest(int[] nums, int start, int end, int k){
        if(start > end)
            return -1;
        int pivot = nums[end], left = start;
        for(int i = start; i < end; i++){
            if(nums[i] < pivot)
                swap(nums, left++, i);
        }
        swap(nums, left, end);
        if(left == k)
            return nums[left];
        return left < k ? findKthLargest(nums, left + 1, end, k) : findKthLargest(nums, 0, left - 1, k);
    }
    private void swap(int[] nums, int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}