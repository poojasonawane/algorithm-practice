/*
Space complexity: O(n)
Time Complexity: O(1)
*/
import java.util.*;

public class ShortestWordDistanceIII {
    public int shortestWordDistance(String[] words, String word1, String word2) {
        int min = words.length, p1 = min, p2 = -min;
        boolean same = word1.equals(word2);
        for(int i = 0; i < words.length; i++){
            if(words[i].equals(word1)){
                if(same){
                    p1 = p2;
                    p2 = i;
                }else{
                    p1 = i;
                }
            }else if(words[i].equals(word2))
                p2 = i;
            min = Math.min(min, Math.abs(p1 - p2));
        }
        return min;
    }
}