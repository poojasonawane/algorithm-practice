/*
Space complexity: O(1)
Time Complexity: O(n)
*/
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class RemoveNthFromEndList {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode nth = null, curr = head;
        while(n > 0){
            curr = curr.next;
            n--;
        }
        if(curr == null)
            return head.next;
        nth = head;
        while(curr.next != null){
            curr = curr.next;
            nth = nth.next;
        }
        nth.next = nth.next.next;
        return head;
    }
}