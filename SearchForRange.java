/*
Space complexity: O(1)
Time Complexity: O(log n)
*/
public class SearchForRange {
    public int[] searchRange(int[] nums, int target) {
        int start = search(nums, target, 0, nums.length);
        if(start == nums.length || nums[start] != target)
            return new int[]{-1, -1};
        return new int[]{start, search(nums, target + 1, start, nums.length)-1};
    }
    private int search(int[] nums, int target, int start, int end){
        while(start < end){
            int mid = (start + end) / 2;
            if(nums[mid] < target){
                start = mid + 1;
            }else{
                end = mid;
            }
        }
        return start;
    }
}