/*
Space complexity: O(n)
Time Complexity: O(n)
*/
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class MergeKSortedLists {
    public ListNode mergeKLists(ListNode[] lists) {
        if(lists.length == 0)
            return null;
        PriorityQueue<ListNode> q = new PriorityQueue<ListNode>(lists.length, new Comparator<ListNode>(){
            public int compare(ListNode a, ListNode b){
                if(a.val < b.val)
                    return -1;
                if(a.val > b.val)
                    return 1;
                return 0;
            }
        });
        for(int i = 0; i < lists.length; i++){
            if(lists[i] != null){
                q.add(lists[i]);
            }
        }
        ListNode dummy = new ListNode(0);
        ListNode tail = dummy;
        while(!q.isEmpty()){
            tail.next = q.poll();
            tail = tail.next;
            if(tail.next != null)
                q.add(tail.next);
        }
        return dummy.next;
    }
}