/*
Space complexity: O(v)
Time Complexity: O(v*e)
*/
public class ConnectedComponentsUndirectedGraph {
    public int countComponents(int n, int[][] edges) {
        int[] v = new int[n];
        for(int i = 0; i < n; i++){
            v[i] = i;
        }
        int res = n;
        for(int[] e : edges){
            int x = find(v, e[0]);
            int y = find(v, e[1]);
            if(x != y){
                v[x] = y;
                res--;
            }
        }
        return res;
    }
    private int find(int[] v, int i){
        while(v[i] != i){
            v[i] = v[v[i]]; //path compression
            i = v[i];
        }
        return i;
    }
}