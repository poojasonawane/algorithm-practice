/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

public class MaxConsecutiveOnesII {
    public int findMaxConsecutiveOnes(int[] nums) {
        int l = 0, h = 0, zero = 0, max = 0;
        for(h = 0; h < nums.length; h++){
            if(nums[h] == 0)
                zero++;
            while(zero > 1){
                if(nums[l++] == 0)
                    zero--;
            }
            max = Math.max(max, h - l + 1);
        }
        return max;
    }
}
/*
streaming input:
public int findMaxConsecutiveOnes(int[] nums) {
	int max = 0, q = -1;
	for (int l = 0, h = 0; h < nums.length; h++) {
		if (nums[h] == 0) {
			l = q + 1;
			q = h;
		}
		max = Math.max(max, h - l + 1);
	}                                                               
	return max;             
}
*/