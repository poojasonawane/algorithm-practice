/*
Space Complexity: O(1)
Time Complexity : O(n)
*/
public class LengthOfLongestSubstring {
    public int lengthOfLongestSubstring(String s) {
        BitSet set = new BitSet(128);
        if(s.length() < 2)
            return s.length();
        int res = 0;
        int start = 0, end = start+1;
        set.set(s.charAt(start));
        while(end < s.length()){
            if(set.get(s.charAt(end))){
                res = Math.max(res, end-start);
                while(s.charAt(start) != s.charAt(end))
                    set.clear(s.charAt(start++));
                start++;
            }else
                set.set(s.charAt(end));
            end++;
        }
        return Math.max(res, end-start);
    }
}

/*
public int lengthOfLongestSubstring(String s) {
        int start = 0, max = 0;
        int[] index = new int[256];
        Arrays.fill(index, -1);
        for(int end = 0; end < s.length(); end++){
            start = Math.max(index[s.charAt(end)]+1, start);
            index[s.charAt(end)] = end;
            max = Math.max(max, end - start + 1);
        }
        return max;
    }
*/