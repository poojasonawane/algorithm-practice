/*
Space complexity: O(mn^3)
Time Complexity: O(mn)
*/
public class ValidSudoku {
    public boolean isValidSudoku(char[][] board) {
        HashSet<String> seen = new HashSet<>();
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[i].length; j++){
                if(board[i][j] != '.'){
                    if(!seen.add(board[i][j] + " in row " + i) || !seen.add(board[i][j] + " in col " + j) || !seen.add(board[i][j] + " in block " + i/3 + "-" + j/3))
                        return false;
                }
            }
        }
        return true;
    }
}