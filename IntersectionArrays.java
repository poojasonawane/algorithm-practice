/*
Space complexity: O(n)
Time Complexity: O(n)
*/
public class IntersectionArrays {
    public int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> set = new HashSet<Integer>();
        HashSet<Integer> set2 = new HashSet<Integer>();
        int i;
        for(i = 0; i < nums1.length; i++){
            set.add(nums1[i]);
        }
        for(i = 0; i < nums2.length; i++){
            if(set.contains(nums2[i]))
                set2.add(nums2[i]);
        }
        int[] res = new int[set2.size()];
        i = 0;
        for(int n : set2){
            res[i++] = n;
        }
        return res;
    }
}

/*
space: O(1)
Time: O(nlogn)
public class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set = new HashSet<>();
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int i = 0;
        int j = 0;
        while (i < nums1.length && j < nums2.length) {
            if (nums1[i] < nums2[j]) {
                i++;
            } else if (nums1[i] > nums2[j]) {
                j++;
            } else {
                set.add(nums1[i]);
                i++;
                j++;
            }
        }
        int[] result = new int[set.size()];
        int k = 0;
        for (Integer num : set) {
            result[k++] = num;
        }
        return result;
    }
}
*/