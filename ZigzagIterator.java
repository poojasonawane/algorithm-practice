public class ZigzagIterator {
    Queue<Iterator> list;
    public ZigzagIterator(List<Integer> v1, List<Integer> v2) {
        list = new LinkedList<Iterator>();
        if(!v1.isEmpty())
            list.offer(v1.iterator());
        if(!v2.isEmpty())
            list.offer(v2.iterator());
    }

    public int next() {
        Iterator i = list.poll();
        int res = (int)i.next();
        if(i.hasNext())
            list.offer(i);
        return res;
    }

    public boolean hasNext() {
        return !list.isEmpty();
    }
}

/**
public class ZigzagIterator {
    Iterator i, j, tmp;
    public ZigzagIterator(List<Integer> v1, List<Integer> v2) {
        i = v2.iterator();
        j = v1.iterator();
    }

    public int next() {
        if(j.hasNext()){
            tmp = j;
            j = i;
            i = tmp;
        }
        return (int)i.next();
    }

    public boolean hasNext() {
        return i.hasNext() || j.hasNext();
    }
}
 */