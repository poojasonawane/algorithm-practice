/*
Space Complexity: O(l)
Time Complexity : O(1)
*/

public class Solution {
    public int hammingDistance(int x, int y) {
        return Integer.bitCount(x ^ y);
    }
}