/*
Space complexity: O(1)
Time Complexity: O(n^3)
*/
public class SparseMatrixMultiply {
    public int[][] multiply(int[][] A, int[][] B) {
        int m = A.length, n = A[0].length, nB = B[0].length; 
        int[][] res = new int[m][nB];
        int i, j, k;
        
        List[] numsA = new List[m];
        for(i = 0; i < m; i++){
            List<Integer> valsA = new ArrayList<>();
            for(j = 0; j < n; j++){
                if(A[i][j] != 0){
                    valsA.add(j);
                }
            }
            numsA[i] = valsA;
        }
        
        for(i = 0; i < numsA.length; i++){
            List<Integer> valsA = numsA[i];
            for(int l = 0; l < valsA.size(); l++){
                k = valsA.get(l);
                for(j = 0; j < nB; j++){
                    res[i][j] += A[i][k] * B[k][j];
                }
            }
        }
        
        return res;
    }
}
/*
public int[][] multiply(int[][] A, int[][] B) {
        int m = A.length, n = A[0].length, nB = B[0].length; 
        int[][] res = new int[m][nB];
        
        for(int i = 0; i < m; i++){
            for(int k = 0; k < n; k++){
                if(A[i][k] != 0){
                    for(int j = 0; j < nB; j++){
                        if(B[k][j] != 0)
                            res[i][j] += A[i][k] * B[k][j];
                    }
                }
            }
        }
        return res;
    }
*/