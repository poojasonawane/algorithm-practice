/*

*/
import java.util.*;

public class MyStack {
    
    Queue<Integer> q1, q2;
    int tail;
    /** Initialize your data structure here. */
    public MyStack() {
        q1 = new LinkedList<Integer>();
        q2 = new LinkedList<Integer>();
    }
    
    /** Push element x onto stack. */
    public void push(int x) {
        q1.offer(x);
        tail = x;
    }
    
    /** Removes the element on top of the stack and returns that element. */
    public int pop() {
        while(q1.size() > 1)
            q2.offer(q1.poll());
        int x = q1.poll();
        while(!q2.isEmpty()){
            tail = q2.poll();
            q1.offer(tail);
        }
        return x;
    }
    
    /** Get the top element. */
    public int top() {
        return tail;
    }
    
    /** Returns whether the stack is empty. */
    public boolean empty() {
        return q1.isEmpty();
    }
}

/**
 * Your MyStack object will be instantiated and called as such:
 * MyStack obj = new MyStack();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.top();
 * boolean param_4 = obj.empty();
 */
 
 
 
 /*
 one queue
 
 public void push(int x) {
    q1.add(x);
    int sz = q1.size();
    while (sz > 1) {
        q1.add(q1.remove());
        sz--;
    }
 
 */