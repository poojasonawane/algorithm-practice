/*
The length of the longest proper prefix in the (sub)pattern that matches a proper suffix in the same (sub)pattern. KMP

Space Complexity: O(1)
Time Complexity : O(n)
*/

public class RepeatedSubstringPattern {
    public boolean repeatedSubstringPattern(String s) {
        int n = s.length();
        int[] prefix = kmp(s);
        int len = prefix[n-1];
        return len > 0 && n%(n-len) == 0;
    }
    private int[] kmp(String s){
        int n = s.length();
        int[] prefix = new int[n];
        prefix[0] = 0;
        int i = 0, j = 1;
        char[] ch = s.toCharArray();
        while(j < n){
            if(ch[i] == ch[j]){
                prefix[j++] = ++i;
            } else {
                if(i == 0)
                    prefix[j++] = 0;
                else
                    i = prefix[i - 1];
            }
        }
        return prefix;
    }
}
/*
Time: O(n^2)
public boolean repeatedSubstringPattern(String s) {
    int l = s.length();
    for(int i = l/2; i > 0; i--){
        if(l % i == 0){
            int m = l / i, j;
            String sb = s.substring(0, i);
            for(j = 1; j < m; j++)
                if(!sb.equals(s.substring(i*j, i+i*j)))
                    break;
            if(m == j)
                return true;
        }
    }
    return false;
}
*/