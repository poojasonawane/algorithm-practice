/*
Space complexity: O(n)
Time Complexity: O(mn)
*/
import java.util.*;

public class SubtreeOfAnotherTree {
    public boolean isSubtree(TreeNode s, TreeNode t) {
        return t == null || s != null && (t.val == s.val && same(s, t) || isSubtree(s.left, t) || isSubtree(s.right, t));
    }
    private boolean same(TreeNode s, TreeNode t){
        return s == null ? t == null : t != null && t.val == s.val && same(s.left, t.left) && same(s.right, t.right);
    }
}
/*
public class SubtreeOfAnotherTree {
    public boolean isSubtree(TreeNode s, TreeNode t) {
        String s1 = inorder(s);
        String s2 = inorder(t);
        System.out.println(s1);
        System.out.println(s2);
        return s1.contains(s2);
    }
    private String inorder(TreeNode t){
        if(t == null)
            return "#";
        String s = "";
        s += "," + t.val;
        s += inorder(t.left);
        s += inorder(t.right);
        
        return s;
    }
}
*/