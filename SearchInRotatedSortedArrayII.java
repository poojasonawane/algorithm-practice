/*
Space complexity: O(1)
Time Complexity: O(log n) worst : O(n)
*/
import java.util.*;

public class SearchInRotatedSortedArrayII {
    public boolean search(int[] nums, int target) {
        if(nums.length == 0)
            return false;
        int l = 0, h = nums.length-1, mid;
        while(l <= h){
            mid = (l+h)/2;
            if(nums[mid] == target)
                return true;
            if(nums[mid] > nums[h]){
                if(nums[mid] > target && nums[l] <= target) 
                    h = mid;
                else 
                    l = mid + 1;
            }else if(nums[mid] < nums[h]){
                if(nums[mid] < target && nums[h] >= target) 
                    l = mid + 1;
                else 
                    h = mid;
            }else{
                h--;
            }
        }
        return nums[l] == target;
    }
}