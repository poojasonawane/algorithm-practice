/*
Space complexity: O(2^n)
Time Complexity: O(n^2)
*/
public class FlipGameII {
    HashMap<String, Boolean> map = new HashMap();
    public boolean canWin(String s) {
        if(s == null || s.length() < 2){
            return false;
        }
        if(map.containsKey(s)){
            return map.get(s);
        }
        for(int i = 0; i <= s.length() - 2; i++){
            if(s.startsWith("++", i)){
                String t = s.substring(0, i) + "--" + s.substring(i + 2);
                if(!canWin(t)){
                    map.put(s, true);
                    return true;
                }
            }
        }
        map.put(s, false);
        return false;
    }
}