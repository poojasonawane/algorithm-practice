/*
Space complexity: O(h)
Time Complexity: O(n)
*/
public class FlattenBTtoLinkedList {
    public void flatten(TreeNode root) {
        Stack<TreeNode> stack = new Stack<TreeNode>();
        if(root == null) 
            return;
        TreeNode curr = root;
        while(curr != null){
            if(curr.left != null && curr.right != null)
                stack.push(curr.right);
            if(curr.left != null){
                curr.right = curr.left;
            }
            if(curr.left == null && curr.right == null && !stack.isEmpty()){
                    curr.right = stack.pop();
            }
            curr.left = null;
            curr = curr.right;
        }
        return;
    }
}

/*
Space complexity: O(1)
Time Complexity: O(n)

void flatten(TreeNode *root) {
		TreeNode*now = root;
		while (now)
		{
			if(now->left)
			{
                //Find current node's prenode that links to current node's right subtree
				TreeNode* pre = now->left;
				while(pre->right)
				{
					pre = pre->right;
				}
				pre->right = now->right;
                //Use current node's left subtree to replace its right subtree(original right 
                //subtree is already linked by current node's prenode
				now->right = now->left;
				now->left = NULL;
			}
			now = now->right;
		}
    }
*/