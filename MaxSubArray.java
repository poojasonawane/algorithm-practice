/*
Space complexity: O(1)
Time Complexity: O(n)
*/
public class MaxSubArray {
    public int maxSubArray(int[] nums) {
        if(nums.length == 0)
            return 0;
        int maxSum = nums[0], maxSoFar = nums[0];
        for(int i = 1; i < nums.length; i++){
            maxSum = Math.max(nums[i], maxSum + nums[i]);
            maxSoFar = Math.max(maxSum, maxSoFar);
        }
        return maxSoFar;
    }
}
/*
DP 
Space: O(n)
public int maxSubArray(int[] A) {
	int n = A.length;
	int[] dp = new int[n];//dp[i] means the maximum subarray ending with A[i];
	dp[0] = A[0];
	int max = dp[0];
	
	for(int i = 1; i < n; i++){
		dp[i] = A[i] + (dp[i - 1] > 0 ? dp[i - 1] : 0);
		max = Math.max(max, dp[i]);
	}
	
    return max;
}
*/