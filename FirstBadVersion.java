/*
Space Complexity: O(1)
Time Complexity : O(log n)
*/
/* The isBadVersion API is defined in the parent class VersionControl.
      boolean isBadVersion(int version); */

public class FirstBadVersion extends VersionControl {
    public int firstBadVersion(int n) {
        int l = 1, h = n, mid;
        while(l < h){
            mid = l + (h - l)/2;
            if(!isBadVersion(mid))
                l = mid + 1;
            else
                h = mid;
        }
        return l;
    }
}