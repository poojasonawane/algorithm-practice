/*
Space complexity: O(p)
Time Complexity: O(s)
*/
import java.util.*;

public class LongestSubstringAtmostTwoDistinctChar {
    public int lengthOfLongestSubstringTwoDistinct(String s) {
        int begin = 0, end = 0, res = 0;
        HashMap<Character, Integer> map = new HashMap();
        int counter = 0;
        while(end < s.length()){
            char ch = s.charAt(end);
            map.put(ch, map.getOrDefault(ch, 0) + 1);
            if(map.get(ch) == 1)
                counter++;
            end++;
            while(counter > 2){
                ch = s.charAt(begin);
                map.put(ch, map.get(ch) - 1);
                if(map.get(ch) == 0)
                    counter--;
                begin++;
            }
            res = Math.max(res, end - begin);
        }
        return res;
    }
}