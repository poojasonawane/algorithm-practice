/*
Space complexity: O(2 ^ n)
Time Complexity: O(2 ^ n)
*/
public class CombinationSum {
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> res = new ArrayList<List<Integer>>();
        helper(candidates, res, target, 0, new ArrayList<Integer>());
        return res;
    }
    private void helper(int[] nums, List<List<Integer>> res, int remain, int pos, ArrayList<Integer> sub){
        if(remain < 0){
            return;
        }else if(remain == 0){
            res.add(new ArrayList<Integer>(sub));
            return;
        }
        for(int i = pos; i < nums.length; i++){
            sub.add(nums[i]);
            helper(nums, res, remain - nums[i], i, sub);
            sub.remove(sub.size()-1);
        }
    }
}