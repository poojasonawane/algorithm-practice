/*
Space complexity: O(n)
Time Complexity: O(n)
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class MostFreqSubTreeSum {
    int max = 0, count;
    public int[] findFrequentTreeSum(TreeNode root) {
        if(root == null){
            return new int[0];
        }
        HashMap<Integer, Integer> vals = new HashMap();
        getSum(root, vals);
        int[] res = new int[count];
        int i = 0;
        for(int key : vals.keySet()){
            if(vals.get(key) == max){
                res[i++] = key;
            }
        }
        return res;
    }
    private int getSum(TreeNode root, HashMap<Integer, Integer> vals){
        if(root == null){
            return 0;
        }
        int lh = getSum(root.left, vals);
        int rh = getSum(root.right, vals);
        int curr = lh + rh + root.val;
        int c = vals.getOrDefault(curr, 0) + 1;
        if(c == max){
            count++;
        }else if(c > max){
            max = c;
            count = 1;
        }
        vals.put(curr, c);
        return curr;
    }
}