/*
Space complexity: O(n)
Time Complexity: O(n)
*/
public class IntersectionArraysII {
    public int[] intersect(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int i = 0, j = 0;
        List<Integer> intersect = new ArrayList();
        while(i < nums1.length && j < nums2.length){
            if(nums1[i] > nums2[j]){
                j++;
            }else if(nums1[i] < nums2[j]){
                i++;
            }else{
                intersect.add(nums1[i++]);
                j++;
            }
        }
        int[] res = new int[intersect.size()];
        i = 0;
        for(int n : intersect){
            res[i++] = n;
        }
        return res;
    }
}