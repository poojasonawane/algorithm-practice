/*
Space complexity: O(n)
Time Complexity: O(n)
*/
import java.util.*;

public class DecodeString {
    public String decodeString(String s) {
        StringBuilder decoded = new StringBuilder();
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) == '[' || s.charAt(i) == ']')
                continue;
            if(Character.isDigit(s.charAt(i))){
                StringBuilder repeatNum = new StringBuilder();
                while(s.charAt(i) != '['){
                    repeatNum.append(s.charAt(i));
                    i++;
                }
                int repeat = Integer.parseInt(repeatNum.toString());
                int j = i+1, brackets = 1;
                StringBuilder encoded = new StringBuilder("[");
                while(brackets != 0){
                    switch(s.charAt(j)){
                        case '[' :
                            brackets++;
                            break;
                        case ']' : 
                            brackets--;
                            break;
                        default :
                    }
                    encoded.append(s.charAt(j));
                    j++;
                }
                for(int k = 0; k < repeat; k++){
                    decoded.append(decodeString(encoded.toString()));
                }
                i = --j;
            } else{
                decoded.append(s.charAt(i));
            }
        }
        return decoded.toString();
    }
}

/*
public class Solution {
    public String decodeString(String s) {
        Stack<Integer> count = new Stack<>();
        Stack<String> result = new Stack<>();
        int i = 0;
        result.push("");
        while (i < s.length()) {
            char ch = s.charAt(i);
            if (ch >= '0' && ch <= '9') {
                int start = i;
                while (s.charAt(i + 1) >= '0' && s.charAt(i + 1) <= '9') i++;
                count.push(Integer.parseInt(s.substring(start, i + 1)));
            } else if (ch == '[') {
                result.push("");
            } else if (ch == ']') {
                String str = result.pop();
                StringBuilder sb = new StringBuilder();
                int times = count.pop();
                for (int j = 0; j < times; j += 1) {
                    sb.append(str);
                }
                result.push(result.pop() + sb.toString());
            } else {
                result.push(result.pop() + ch);
            }
            i += 1;
        }
        return result.pop();
    }
}
*/