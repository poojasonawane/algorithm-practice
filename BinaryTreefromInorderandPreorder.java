/*
Space complexity: O(n)
Time Complexity: O(n)
*/
public class BinaryTreefromInorderandPreorder {
    public TreeNode buildTree(int[] preorder, int[] inorder) {
       return build(inorder, 0 , inorder.length-1, preorder, 0, preorder.length-1);
    }
    public TreeNode build(int[] inorder, int inStart, int inEnd, int[] preorder, int preStart, int preEnd){
        if(inStart > inEnd || preStart > preEnd)
            return null;
        TreeNode curr = new TreeNode(preorder[preStart]);
        int offset;
        for(offset = inStart; offset < inEnd; offset++){
            if(inorder[offset] == curr.val)
                break;
        }
        curr.left = build(inorder, inStart, offset-1, preorder, preStart+1, preStart+offset-inStart);
        curr.right = build(inorder, offset+1, inEnd, preorder, preStart+offset-inStart+1, preEnd);
        return curr; 
    }
}