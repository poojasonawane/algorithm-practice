/*
Space complexity: O(p)
Time Complexity: O(s)
*/
import java.util.*;

public class FindAllAnagramsInString {
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> res = new ArrayList<Integer>();
        if(p.length() > s.length())
            return res;
        int begin = 0, end = 0;
        HashMap<Character, Integer> map = new HashMap();
        for(Character ch : p.toCharArray()){
            map.put(ch, map.getOrDefault(ch, 0) + 1);
        }
        int counter = map.size();
        while(end < s.length()){
            char ch = s.charAt(end);
            if(map.containsKey(ch)){
                map.put(ch, map.get(ch)-1);
                if(map.get(ch) == 0)
                    counter--;
            }
            end++;
            while(counter == 0){
                ch = s.charAt(begin);
                if(map.containsKey(ch)){
                    map.put(ch, map.get(ch)+1);
                    if(map.get(ch) > 0)
                        counter++;
                    if(end - begin == p.length())
                        res.add(begin);
                }
                begin++;
            }
        }
        return res;    
    }
}