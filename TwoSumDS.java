/*
Space complexity: O(n)
Time Complexity: O(n)
*/
import java.util.*;

public class TwoSum {

    /** Initialize your data structure here. */
    HashMap<Integer, Integer> map;
    List<Integer> nums;
    public TwoSum() {
        map = new HashMap<>();
        nums = new ArrayList<Integer>();
    }
    
    /** Add the number to an internal data structure.. */
    public void add(int number) {
        map.put(number, map.getOrDefault(number, 0) + 1);
    }
    
    /** Find if there exists any pair of numbers which sum is equal to the value. */
    public boolean find(int value) {
        for(int key : map.keySet()){
            int diff = value - key;
            if((diff != key && map.containsKey(diff)) || (diff == key && map.get(key) > 1))
                return true;
        }
        return false;
    }
}

/**
 * Your TwoSum object will be instantiated and called as such:
 * TwoSum obj = new TwoSum();
 * obj.add(number);
 * boolean param_2 = obj.find(value);
 */