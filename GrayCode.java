/*
Space complexity: O(n)
Time Complexity: O(1)
*/
public class GrayCode {
    public List<Integer> grayCode(int n) {
        List<Integer> res = new ArrayList<Integer>();
        int count = 1 << n;
        for(int i = 0; i < count; i++){
            res.add(i ^ (i >> 1));
        }
        return res;
    }
}
/*
 int nums = 0;
    public List<Integer> grayCode(int n) {
        List<Integer> ret = new ArrayList<>();
        backTrack(n, ret);
        return ret;
    }
    
    private void backTrack(int n, List<Integer> ret) {
        if (n == 0) {
            ret.add(nums);
            return;
        }
        else {
            backTrack(n - 1, ret);
            nums ^= (1 << n - 1);
            backTrack(n - 1, ret);
        }
    }
*/