/*
Space Complexity: O(1)
Time Complexity : O(n^2)
*/

public class ValidWordSquare {
    public boolean validWordSquare(List<String> words) {
        int m = words.size(), n = words.get(0).length();
        if (m!=n) return false;
        for (int i=1; i<m; i++) {
            if (m < words.get(i).length()) return false;
            for (int j=0; j<i; j++) {
                if (!isSame(words, i, j)) return false;
            }
        }
        return true;
    }
    private boolean isSame(List<String> words, int i, int j) {
        if (j >= words.size()) return false;
        String a = words.get(i);
        char ac = j < a.length() ? a.charAt(j) : ' ';
        String b = words.get(j);
        char bc = i < b.length() ? b.charAt(i) : ' ';
        return ac == bc;
    }
}
/*
public boolean validWordSquare(List<String> words) {
    for(int i = 0; i < words.size(); i++){
        int n = words.get(i).length();
        for(int j = 0; j < n; j++)
            if(j >= words.size() || words.get(j).length() <= i || words.get(i).charAt(j) != words.get(j).charAt(i))
                return false;
    }
    return true;
}
*/