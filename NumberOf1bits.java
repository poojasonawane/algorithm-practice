/*
Space Complexity: O(l)
Time Complexity : O(log n) s is size of int
*/

public class NumberOf1bits {
    // you need to treat n as an unsigned value
    public int hammingWeight(int n) {
        int res = 0;
        while(n != 0){
            res++;
            n &= n - 1;
        }
        return res;
    }
}
/*
return Integer.bitCount(n ^ 0);
*/