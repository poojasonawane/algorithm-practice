/*
Space complexity: O(1)
Time Complexity: O(log n)
*/
import java.util.*;

public class Power {
    public double myPow(double x, int n) {
        double res=1;
        while(n!=0){
            if(n%2==0){
                x=x*x;
                n/=2;
            }else{
                if(n>0){
                    res*=x;
                    n--;
                }else{
                    res/=x;
                    n++;
                }
            }
        }
        return res;
    }
}

/*
Space: 0(1)
public double myPow(double x, int n) {
        double ans = 1;
        long absN = Math.abs((long)n);
        while(absN > 0) {
            if((absN&1)==1) ans *= x;
            absN >>= 1;
            x *= x;
        }
        return n < 0 ?  1/ans : ans;
    }
*/