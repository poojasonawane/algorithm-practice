/*
Space Complexity: O(1)
Time Complexity : O(n)
*/

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class LinkedListPalindrome {
    public boolean isPalindrome(ListNode head) {
        ListNode slow = head, fast = head, prev = null, next;
        while(fast != null && fast.next != null){
            fast = fast.next.next;
            next = slow.next;
            slow.next = prev;
            prev = slow;
            slow = next;
        }
        ListNode second = prev;
        prev = slow;
        slow = fast == null ? slow : slow.next;
        while(slow != null && slow.val == second.val){
            slow = slow.next;
            /*next = second.next;
            second.next = prev;
            prev = second;*/
            second = second.next;
        }
        return second == null;
    }
}