/*
Space complexity: O(1)
Time Complexity: O(n*len)
*/
public class CountAndSay {
    public String countAndSay(int n) {
        StringBuilder curr = new StringBuilder("1"), prev;
        for(int i = 2; i <= n; i++){
            prev = curr;
            curr = new StringBuilder();
            int count = 1;
            int j = 1;
            for(; j < prev.length(); j++){
                if(prev.charAt(j) != prev.charAt(j-1)){
                    curr.append(count).append(prev.charAt(j-1));
                    count = 1;
                }else{
                    count++;
                }
            }
            curr.append(count).append(prev.charAt(j-1));
        }
        return curr.toString();
    }
}