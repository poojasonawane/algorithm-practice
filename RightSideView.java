/*
Space complexity: O(w)
Time Complexity: O(n)
*/
public class RightSideView {
    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        if(root == null)
            return result;
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        queue.offer(null);
        while(queue.size() > 1){
            TreeNode curr = queue.poll();
            if(curr == null){
                queue.offer(curr);
                continue;
            }
            if(queue.peek() == null)
                result.add(curr.val);
            if(curr.left != null) 
                queue.offer(curr.left);
            if(curr.right != null) 
                queue.offer(curr.right);
        }
        return result;
    }
}