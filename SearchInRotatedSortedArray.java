/*
Space Complexity: O(1)
Time Complexity : O(log n)
*/
public class SearchInRotatedSortedArray {
    public int search(int[] nums, int target) {
        if(nums.length == 0)
            return -1;
        int l = 0, h = nums.length-1, mid;
        while(l < h){
            mid = (l+h)/2;
            if(nums[mid] == target)
                return mid;
            if(nums[mid] >= nums[l]){
                if(target >= nums[l] && target < nums[mid])
                    h = mid - 1;
                else
                    l = mid + 1;
            }else{
                if(target > nums[mid] && target <= nums[h])
                    l = mid + 1;
                else
                    h = mid - 1;
            }
        }
        return nums[h] == target ? h : -1;
    }
}