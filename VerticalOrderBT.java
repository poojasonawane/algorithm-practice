/*
Space complexity: O(n)
Time Complexity: O(n)
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class VerticalOrderBT {
    public List<List<Integer>> verticalOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        if(root == null)
            return result;
        HashMap<Integer, List<Integer>> map = new HashMap<Integer, List<Integer>>();
        Queue<TreeNode> bfs = new LinkedList<TreeNode>();
        Queue<Integer> cols = new LinkedList<Integer>();
        bfs.offer(root);
        cols.offer(0);
        int min = 0, max = 0;
        while(!bfs.isEmpty()){
            int col = cols.poll();
            TreeNode curr = bfs.poll();
            List<Integer> vals = map.getOrDefault(col, new ArrayList<Integer>());
            vals.add(curr.val);
            map.put(col, vals);
            
            if(curr.left != null){
                bfs.offer(curr.left);
                cols.offer(col-1);
                min = Math.min(col-1, min);
            }
            if(curr.right != null){
                bfs.offer(curr.right);
                cols.offer(col+1);
                max = Math.max(col+1, max);
            }
        }
        for(int i = min; i <= max; i++){
            result.add(map.get(i));
        }
        return result;
    }
}