/*
Space complexity: O(2 ^ n)
Time Complexity: O(2 ^ n)
*/
public class Subsets {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<List<Integer>>();
        generateSets(nums, res, new ArrayList<Integer>(), 0);
        return res;
    }
    private void generateSets(int[] nums, List<List<Integer>> res, List<Integer> sub, int start){
        res.add(new ArrayList<Integer>(sub));
        for(int i = start; i < nums.length; i++){
            sub.add(nums[i]);
            generateSets(nums, res, sub, i+1);
            sub.remove(sub.size()-1);
        }
    }
}