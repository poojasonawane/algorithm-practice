/*
Space Complexity: O(1)
Time Complexity : O(1)
*/
public class PowerOfThree {
    public boolean isPowerOfThree(int n) {
        return n > 0 && 1162261467 % n == 0;
    }
}

/*
public boolean isPowerOfThree(int n) {
        return Integer.toString(n, 3).matches("^10*$");
    }
*/