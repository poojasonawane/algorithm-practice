/*
Space complexity: O(2 ^ n)
Time Complexity: O(2 ^ n)
*/
public class CombinationSum3 {
    public List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> res = new ArrayList<List<Integer>>();
        helper(res, k, n, 1, new ArrayList<Integer>());
        return res;
    }
    private void helper(List<List<Integer>> res, int k, int remain, int pos, ArrayList<Integer> sub){
        if(remain < 0){
            return;
        }else if(remain == 0 && sub.size() == k){
            res.add(new ArrayList<Integer>(sub));
            return;
        }
        for(int i = pos; i < 10; i++){
            sub.add(i);
            helper(res, k, remain - i, i+1, sub);
            sub.remove(sub.size()-1);
        }
    }
}