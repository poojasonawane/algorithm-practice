/*
Space complexity: O(t)
Time Complexity: O(n * t)
*/
public class CombinationSum4 {
    public int combinationSum4(int[] nums, int target) {
        int[] dp = new int[target+1];
        Arrays.fill(dp, -1);
        return helper(nums, target, dp);
    }
    private int helper(int[] nums, int target, int[] dp) {
        if(target <= 0){
            return target == 0 ? 1 : 0;
        }
        if(dp[target] != -1){
            return dp[target];
        }
        int res = 0;
        for(int n : nums){
            res += helper(nums, target-n, dp);
        }
        dp[target] = res;
        return res;
    }
}