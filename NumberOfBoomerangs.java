/*
Space Complexity: O(n)
Time Complexity : O(n^2)
*/

public class NumberOfBoomerangs {
    public int numberOfBoomerangs(int[][] points) {
        HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
        int res = 0;
        for(int i = 0; i < points.length; i++){
            m.clear();
            for(int j = 0; j < points.length; j++){
                if(i != j){
                    int d = distance(points[i], points[j]);
                    m.put(d, m.getOrDefault(d, 0) + 1);
                }
            }
            for(int val : m.values())
                if(val > 1)
                    res += val * (val - 1);
        }
        return res;
    }
    private int distance(int[] x, int[] y){
        int dx = x[0] - y[0];
        int dy = x[1] - y[1];
        return dx*dx + dy*dy;
    }
}