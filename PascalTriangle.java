/*
Space complexity: O(1)
Time Complexity: O(n ^ 2)
*/
import java.util.*;

public class PascalTriangle {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> res = new ArrayList();
        List<Integer> curr = new ArrayList();
        for(int i = 0; i < numRows; i++){
            curr.add(1);
            for(int j = i-1; j > 0; j--){
                curr.set(j, curr.get(j - 1) + curr.get(j));
            }
            res.add(new ArrayList(curr));
        }
        return res;
    }
}