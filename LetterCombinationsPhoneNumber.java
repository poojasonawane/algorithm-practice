/*
Space complexity: O(1)
Time Complexity: O(n)
*/
public class LetterCombinationsPhoneNumber {
    public List<String> letterCombinations(String digits) {
        LinkedList<String> res = new LinkedList<>();
        if(digits.length() == 0)
            return res;
        String[] mapping = new String[] {"0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        res.add("");
        for(int i = 0; i < digits.length(); i++){
            int d = Character.getNumericValue(digits.charAt(i));
            while(res.peek().length() == i){
                String p = res.remove();
                for(char c : mapping[d].toCharArray()){
                    res.add(p + c);
                }
            }
        }
        return res;
    }
}