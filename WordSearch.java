/*
Space complexity: O(n)
Time Complexity: O(4^n)
*/
import java.util.*;

public class WordSearch {
    public boolean exist(char[][] board, String word) {
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[0].length; j++){
                if(check(board, word, i, j, 0))
                    return true;
            }
        }
        return false;
    }
    private boolean check(char[][] board, String word, int i, int j, int start){
        if(start == word.length())
            return true;
        if(i < 0 || j < 0 || i >= board.length || j >= board[i].length || board[i][j] != word.charAt(start))
            return false;
        board[i][j] ^= 256;
        boolean exist =  check(board, word, i+1, j, start+1) 
                        || check(board, word, i, j+1, start+1) 
                        || check(board, word, i-1, j, start+1) 
                        || check(board, word, i, j-1, start+1);
        board[i][j] ^= 256; 
        return exist;
    }
}