/*
Space Complexity: O(1)
Time Complexity : O(1)
*/
public class PowerOfTwo {
    public boolean isPowerOfTwo(int n) {
        return n > 0 && (n & (n - 1)) == 0;
    }
}