/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class ConstructBTfromString {
    public TreeNode str2tree(String s) {
        if(s == null || s.length() == 0)
            return null;
        int firstParen = s.indexOf("(");
        TreeNode node;
        if(firstParen == -1){
            node = new TreeNode(Integer.parseInt(s));
            return node;
        }
        node = new TreeNode(Integer.parseInt(s.substring(0, firstParen)));
        int leftParen = 0, start = firstParen;
        for(int i = firstParen; i < s.length(); i++){
            if(s.charAt(i) == '(')
                leftParen++;
            else if(s.charAt(i) == ')')
                leftParen--;
            if(leftParen == 0 && start == firstParen){
                node.left = str2tree(s.substring(start+1, i));
                start = i+1;
            }
            else if(leftParen == 0)
                node.right = str2tree(s.substring(start+1, i));
        }
        return node;
    }
}