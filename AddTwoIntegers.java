/*
Space complexity: O(1)
Time Complexity: O(n)
*/
public class AddTwoIntegers {
    public int getSum(int a, int b) {
        return b == 0 ? a : getSum(a ^ b, (a & b) << 1);   
    }
}