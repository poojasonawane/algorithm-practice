/*
Space complexity: O(w)
Time Complexity: O(n)
*/
public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        StringBuilder result = new StringBuilder();
        if(root == null)
            return null;
        queue.offer(root);
        while(!queue.isEmpty()){
            int size = queue.size();
            while(size != 0){
                TreeNode curr = queue.poll();
                if(curr != null){
                    result.append(Integer.toString(curr.val));
                    queue.offer(curr.left);
                    queue.offer(curr.right);
                }
                else
                    result.append("n");
                result.append(",");
                size--;
            }
        }
        result.deleteCharAt(result.lastIndexOf(","));
        System.out.println(result.toString());
        return result.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        TreeNode root = null;
        if(data == null)
            return null;
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        String[] tokens = data.split(",");
        root = new TreeNode(Integer.parseInt(tokens[0]));
        queue.offer(root);
        for(int i = 1; i < tokens.length-1; i++){
            TreeNode curr = queue.poll();
            if (!tokens[i].equals("n")) {
                curr.left = new TreeNode(Integer.parseInt(tokens[i]));
                queue.add(curr.left);
            }
            if (!tokens[++i].equals("n")) {
                curr.right = new TreeNode(Integer.parseInt(tokens[i]));
                queue.add(curr.right);
            }
        }
        return root;
    }
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.deserialize(codec.serialize(root));