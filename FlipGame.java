/*
Space complexity: O(1)
Time Complexity: O(n)
*/
public class FlipGame {
    public List<String> generatePossibleNextMoves(String s) {
        List<String> res = new ArrayList<String>();
        for(int i = -1; (i = s.indexOf("++", i+1)) >= 0; ){
            res.add(s.substring(0, i) + "--" + s.substring(i+2, s.length()));
        }
        return res;
    }
}