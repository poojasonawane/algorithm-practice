/*
Space complexity: O(1)
Time Complexity: O(n + m)
*/
public class AddStrings {
    public String addStrings(String num1, String num2) {
        int c = 0;
        StringBuilder res = new StringBuilder();
        for(int i = num1.length() - 1, j = num2.length() - 1; i >= 0 || j >= 0;){
            int sum = 0;
            if(i >= 0){
                sum += num1.charAt(i--) - '0';
            }
            if(j >= 0){
                sum += num2.charAt(j--) - '0';
            }
            sum += c;
            c = sum / 10;
            res.append(Integer.toString(sum % 10));
        }
        if(c > 0){
            res.append(Integer.toString(c));
        }
        return res.reverse().toString();
    }
}