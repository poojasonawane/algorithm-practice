/*
Space complexity: O(n)
Time Complexity: O(n)
*/
import java.util.*;

public class ReverseVowels {
    public String reverseVowels(String s) {
        char[] cs = s.toCharArray();
        String vowels = "aeiouAEIOU";
        int begin = 0, end = s.length()-1;
        while(begin < end){
            while(begin < s.length() && vowels.indexOf(cs[begin]) == -1) //!vowels.contains(cs[start]+"")
                begin++;
                //if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')
            while(end >= 0 && vowels.indexOf(cs[end]) == -1)
                end--;
            if(begin < end){
                char temp = cs[begin];
                cs[begin] = cs[end];
                cs[end] = temp;
                begin++;
                end--;
            }
        }
        return new String(cs);
    }
}