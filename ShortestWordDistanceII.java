/*
Space complexity: O(n)
Time Complexity: O(n)
*/
import java.util.*;

public class ShortestWordDistanceII {
    HashMap<String, List<Integer>> map;
    
    public WordDistance(String[] words) {
        map = new HashMap<>();
        for(int i = 0; i < words.length; i++){
            if(!map.containsKey(words[i])){
                map.put(words[i], new ArrayList<Integer>());
            }
            map.get(words[i]).add(i);
        }
    }
    
    public int shortest(String word1, String word2) {
        int min = Integer.MAX_VALUE;
        List<Integer> is = map.get(word1);
        List<Integer> js = map.get(word2);
        int i = 0, j = 0;
        while(i < is.size() && j < js.size()){
            int t1 = is.get(i);
            int t2 = js.get(j);
            if(t1 < t2){
                min = Math.min(min, t2 - t1);
                i++;
            }else{
                min = Math.min(min, t1 - t2);
                j++;
            }
        }
        return min;
    }
}

/**
 * Your WordDistance object will be instantiated and called as such:
 * WordDistance obj = new WordDistance(words);
 * int param_1 = obj.shortest(word1,word2);
 */