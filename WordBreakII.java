/*
Space complexity: O(1)
Time Complexity: O(len(dict)^len(word))
*/
public class WordBreakII {
    public List<String> wordBreak(String s, List<String> wordDict) {
        return DFS(s, wordDict, new HashMap<String, List<String>>());
    }
    private List<String> DFS(String s, List<String> wordDict, HashMap<String, List<String>> map){
        List<String> res = new ArrayList<String>();
        if(s.length() == 0){
            res.add("");
            return res;
        }
        if(map.containsKey(s))
            return map.get(s);
        for(String word : wordDict){
            if(s.startsWith(word)){
                List<String> sub = DFS(s.substring(word.length()), wordDict, map);
                for(String breaked : sub){
                    res.add(word + (breaked.isEmpty() ? "" : " ") + breaked);
                }
            }
        }
        map.put(s, res);
        return res;
    }
}