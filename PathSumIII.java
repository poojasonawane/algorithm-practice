/*
Space complexity: O(log n) for balanced tree else O(n)
Time Complexity: O(n)
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class PathSumIII {
    public int pathSum(TreeNode root, int sum) {
        return getPathSum(root, sum, 0, new HashMap<Integer, Integer>());
    }
    private int getPathSum(TreeNode root, int targetSum, int runningSum, HashMap<Integer, Integer> map){
        if(root == null)
            return 0;
        runningSum += root.val;
        int sum = runningSum - targetSum;
        int totalPaths = map.getOrDefault(sum, 0);
        if(runningSum == targetSum)
            totalPaths++;
        updateMap(map, runningSum, 1);
        totalPaths += getPathSum(root.left, targetSum, runningSum, map);
        totalPaths += getPathSum(root.right, targetSum, runningSum, map);
        updateMap(map, runningSum, -1);
        return totalPaths;
    }
    private void updateMap(HashMap<Integer, Integer> map, int k, int delta){
        int val = map.getOrDefault(k, 0) + delta;
        if(val == 0)
            map.remove(k);
        else
            map.put(k, val);
    }
}
/*
Time Complexity: O(n^2)
Space Complexity
Recursive: 
public int pathSum(TreeNode root, int sum) {
    if (root == null) return 0;
    return pathSumFrom(root, sum) + pathSum(root.left, sum) + pathSum(root.right, sum);
}

private int pathSumFrom(TreeNode node, int sum) {
    if (node == null) return 0;
    return (node.val == sum ? 1 : 0) 
        + pathSumFrom(node.left, sum - node.val) + pathSumFrom(node.right, sum - node.val);
}
*/