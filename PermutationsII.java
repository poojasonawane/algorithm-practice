/*
Space complexity: O(2 ^ n)
Time Complexity: O(2 ^ n)
*/
public class PermutationII {
    public List<List<Integer>> permuteUnique(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> res = new ArrayList<List<Integer>>();
        backtrack(nums, res, new ArrayList<Integer>(), new boolean[nums.length]);
        return res;
    }
    private void backtrack(int[] nums, List<List<Integer>> res, List<Integer> sub, boolean[] visited){
        if(sub.size() == nums.length){
            res.add(new ArrayList<Integer>(sub));
        }else{
            for(int i = 0; i < nums.length; i++){
                if(visited[i] || i != 0 && nums[i] == nums[i-1] && !visited[i-1]) continue;
                visited[i] = true;
                sub.add(nums[i]);
                backtrack(nums, res, sub, visited);
                visited[i] = false;
                sub.remove(sub.size()-1);
            }
        }
    }
}