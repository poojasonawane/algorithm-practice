/*
The basic idea comes from the description h of his/her N papers have at least h citations each.
h-index is defined as the number of papers with reference greater than the number.
Then we iterate from the back to the front of the buckets, whenever the total count exceeds the index of the bucket, meaning that we have the index number of papers that have reference greater than or equal to the index. Which will be our h-index result. The reason to scan from the end of the array is that we are looking for the greatest h-index.

Space complexity: O(1)
Time Complexity: O(log n)
*/
public class HIndexII {
    public int hIndex(int[] citations) {
    	int len = citations.length;
    	int lo = 0, hi = len - 1;
    	while (lo <= hi) {
    		int med = (hi + lo) / 2;
    		if (citations[med] == len - med) {
    			return len - med;
    		} else if (citations[med] < len - med) {
    			lo = med + 1;
    		} else { 
    			//(citations[med] > len-med), med qualified as a hIndex,
    		    // but we have to continue to search for a higher one.
    			hi = med - 1;
    		}
    	}
    	return len - lo;
    }
}