/*
Space Complexity: O(1)
Time Complexity : O(nlogn + mlogm)
*/

public class FindRadius {
    public int findRadius(int[] houses, int[] heaters) {
        int min = 0, j = 0;
        Arrays.sort(heaters);
        Arrays.sort(houses);
        for(int house : houses){
            while(j < heaters.length-1 && heaters[j] + heaters[j+1] <= house * 2)
                j++;
            min = Math.max(min, Math.abs(house-heaters[j]));
        }
        return min;
    }
}