/*
Space complexity: O(1)
Time Complexity: O(n)
*/
public class ProductExceptSelf {
    public int[] productExceptSelf(int[] nums) {
        int[] res = new int[nums.length];
        res[nums.length-1] = 1;
        int i;
        for(i = nums.length-2; i >= 0; i--){
            res[i] = res[i+1] * nums[i+1];
        }
        int leftCum = 1;
        for(i = 0; i < nums.length; i++){
            res[i] = res[i] * leftCum;
            leftCum *= nums[i];
        }
        return res;
    }
}