/*
Space complexity: O(1)
Time Complexity: O(log n)
*/
import java.util.*;

public class Sqrt {
    public int mySqrt(int x) {
         long r = x;
        while (r*r > x)
            r = (r + x/r) / 2;
        return (int) r;
    }
}