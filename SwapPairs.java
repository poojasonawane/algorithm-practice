/*
Space Complexity: O(1)
Time Complexity : O(n)
*/
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class SwapPairs {
    public ListNode swapPairs(ListNode head) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode prev = dummy;
        while(prev.next != null && prev.next.next != null){
            ListNode a = prev.next;
            ListNode b = a.next;
            a.next = b.next;
            b.next = a;
            prev.next = b;
            prev = a;
        }
        return dummy.next;
    }
}
/*
Space : O(n)
public ListNode swapPairs(ListNode head) {
        if(head == null || head.next == null)
            return head;
            ListNode n = head.next;
            head.next = swapPairs(n.next);
            n.next = head;
        return n;
    }
*/