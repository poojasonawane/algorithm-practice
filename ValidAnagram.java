/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

public class ValidAnagram {
    public boolean isAnagram(String s, String t) {
        if(s.length() != t.length())
            return false;
        int[] map = new int[256];
        int i = 0;
        while(i < s.length()){
            map[(int)s.charAt(i)]++;
            map[(int)t.charAt(i++)]--;
        }
        i = 0;
        while(i < 256){
           if(map[i++] != 0)
                return false;
        }
        return true;
    }
}