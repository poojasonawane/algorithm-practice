/*
Space complexity: O(1)
Time Complexity: O(log n)
*/
import java.util.*;

public class ValidPerfectSquare {
    public boolean isPerfectSquare(int num) {
        long r = num;
        while(r * r > num)
            r = (r + num / r) / 2;
        return r * r == num;
    }
}