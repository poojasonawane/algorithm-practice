/*
Space complexity: O(n)
Time Complexity: O(1)
*/
import java.util.*;

public class ValidParentheses {
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<Character>();
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) == '(' || s.charAt(i) == '[' || s.charAt(i) == '{')
                stack.push(s.charAt(i));
            else if(stack.isEmpty() || ((s.charAt(i) == ')' && stack.pop() != '(') || (s.charAt(i) == '}' && stack.pop() != '{') || (s.charAt(i) == ']' && stack.pop() != '[')))
                return false;
        }
        
        return stack.isEmpty();
    }
}




/*
 * Complete the function below.
 */

    static String[] braces(String[] values) {
        String[] result = new String[values.length];
        int k = 0;
        for(int j = 0; j < values.length; j++){
            Stack<Character> stack = new Stack<Character>();
            for(int i = 0; i < values[j].length(); i++){
                if(values[j].charAt(i) == '(')
                    stack.push(')');
                else if(values[j].charAt(i) == '{')
                    stack.push('}');
                else if(values[j].charAt(i) == '[')
                    stack.push(']');
                else if(stack.isEmpty() || values[j].charAt(i) != stack.pop()){
                    result[k++] = "NO";
                    break;
                }
                if(i == values[j].length() - 1){
					if(stack.isEmpty())
						result[k++] = "YES";
					else
						result[k++] = "NO";
				}
            }  
        }
        return result;
    }

