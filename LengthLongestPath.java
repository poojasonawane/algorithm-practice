/*
Space Complexity: O(l)
Time Complexity : O(n)
*/

public class LengthLongestPath {
    public int lengthLongestPath(String input) {
        String[] paths = input.split("\n");
        int[] stack = new int[paths.length+1];
        int maxLen = 0;
        for(String s:paths){
            int lev = s.lastIndexOf("\t")+1, curLen = stack[lev+1] = stack[lev]+s.length()-lev+1;
            //System.out.println("len: "+s.length()+" currLen: "+ curLen + " lev: "+ lev + " stack[l]: "+ stack[lev]+ " stack[l+1]: "+ stack[lev+1]);
            if(s.contains(".")) maxLen = Math.max(maxLen, curLen-1);
        }
        return maxLen;
    }
}