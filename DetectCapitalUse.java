/*
Space Complexity: O(1)
Time Complexity : O(n)
*/

public class DetectCapitalUse {
    public boolean detectCapitalUse(String word) {
        return word.matches("[A-Z]+|[A-Z]?[a-z]+");
    }
}

/*
public boolean detectCapitalUse(String word) {
        int cnt = 0;
        for(char c: word.toCharArray()) if('Z' - c >= 0) cnt++;
        return ((cnt==0 || cnt==word.length()) || (cnt==1 && 'Z' - word.charAt(0)>=0));
    }
*/