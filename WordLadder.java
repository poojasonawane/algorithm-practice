/*
Space complexity: O(n)
Time Complexity: O(n)
*/
import java.util.*;

public class Solution {
    public int ladderLength(String beginWord, String endWord, List<String> words) {
        HashSet<String> wordList = new HashSet<>(words);
        if(!wordList.contains(endWord))
            return 0;
        HashSet<String> beginSet = new HashSet<>();
        HashSet<String> endSet = new HashSet<>();
        HashSet<String> visitedSet = new HashSet<>();
        beginSet.add(beginWord);
        endSet.add(endWord);
        int len = 1, strln = endWord.length();
        while(!beginSet.isEmpty() && !endSet.isEmpty()){
            if(beginSet.size() > endSet.size()){
                HashSet<String> temp = beginSet;
                beginSet = endSet;
                endSet = temp;
            }
            HashSet<String> next = new HashSet<>();
            for(String word : beginSet){
                char[] charSet = word.toCharArray();
                for(int i = 0; i < strln; i++){
                    for(char ch = 'a'; ch <= 'z'; ch++){
                        char old = charSet[i];
                        charSet[i] = ch;
                        String target = String.valueOf(charSet);
                        if(endSet.contains(target)){
                            return len + 1;
                        }
                        if(!visitedSet.contains(target) && wordList.contains(target)){
                            visitedSet.add(target);
                            next.add(target);
                            wordList.remove(target);
                        }
                        charSet[i] = old;
                    }
                }
            }
            beginSet = next;
            len++;
        }
        return 0;
    }
}