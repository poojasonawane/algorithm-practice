/*
Space complexity: O(1)
Time Complexity: O(mn log mn)
*/
import java.util.*;

public class Search2dMatrix {
    public boolean searchMatrix(int[][] matrix, int target) {
        if(matrix == null || matrix.length < 1 || matrix[0].length < 1){
            return false;
        }
        int n = matrix.length;
        int m = matrix[0].length;
        int l = 0, h = n * m - 1;
        while(l <= h){
            int mid = (l + h) >> 1;
            if(matrix[mid / m][mid % m] == target){
                return true;
            }
            if(matrix[mid / m][mid % m] < target){
                l = mid + 1;
            }else
                h = mid - 1;
        }
        return h > 0 && matrix[h / m][h % m] == target;
    }
}