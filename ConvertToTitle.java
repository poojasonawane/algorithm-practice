/*
Space Complexity: O(l)
Time Complexity : O(log n) s is size of int
*/

public class ConvertToTitle {
    public String convertToTitle(int n) {
        StringBuilder res = new StringBuilder();
        while(n > 0){
            res.append((char)(--n%26 + 'A'));
            n = n/26;
        }
        return res.reverse().toString();
    }
}
/*
return n == 0 ? "" : convertToTitle(--n / 26) + (char)('A' + (n % 26));
        
*/