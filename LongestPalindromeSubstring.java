/*
http://articles.leetcode.com/longest-palindromic-substring-part-ii : o(n)
Space Complexity: O(1)
Time Complexity : O(n^2)
*/
public class LongestPalindromeSubstring {
    public String longestPalindrome(String s) {
        String res = "";
        if(s.length()==0)
            return res;
        int maxLen = 0;
        for(int i = 0; i < s.length(); i++){
            if(check(s, i - maxLen - 1, i)){
                res = s.substring(i - maxLen - 1, i + 1);
                maxLen += 2;
            }else if(check(s, i - maxLen, i)){
                res = s.substring(i - maxLen, i+1);
                maxLen += 1;
            }
        }
        return res;
    }
    private boolean check(String s, int begin, int end){
        if(begin < 0)
            return false;
        while(begin < end)
            if(s.charAt(begin++) != s.charAt(end--))
                return false;
        return true;
    }
}