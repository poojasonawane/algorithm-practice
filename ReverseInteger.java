/*
Space complexity: O(1)
Time Complexity: O(b)
*/
import java.util.*;

public class ReverseInteger {
    public int reverse(int x) {
        int result = 0;
        while(x != 0){
            int temp = result * 10 + (x % 10);
            if((temp - (x % 10)) / 10 != result)
                return 0;
            x = x / 10;
            result = temp;
        }
        return result;
    }
}