/*
Space complexity: O(f)
Time Complexity: O(n)
*/
import java.util.*;

public class SimplifyPath {
    public String simplifyPath(String path) {
        Deque<String> q = new LinkedList<>();
        HashSet<String> skip = new HashSet<>(Arrays.asList("", ".", ".."));
        String[] dirs = path.split("/");
        for(String dir : dirs){
            if(dir.equals("..") && !q.isEmpty())
                q.pop();
            if(!skip.contains(dir))
                q.push(dir);
        }
        String res = "";
        for(String dir : q){
            res = "/" + dir + res;
        }
        return q.isEmpty() ? "/" : res;
    }
}