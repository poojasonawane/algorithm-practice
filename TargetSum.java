/*
Space complexity: O(sum)
Time Complexity: O(n * sum)
*/
public class TargetSum {
    public int findTargetSumWays(int[] nums, int S) {
        int sum = S;
        for(int n : nums){
            sum += n;
        }
        if(sum % 2 == 1 || sum < 2*S){
            return 0;
        }
        sum /= 2;
        int[] sack = new int[sum+1];
        sack[0] = 1;
        subsetSum(nums, sack, sum);
        return sack[sum];
    }
    private void subsetSum(int[] nums, int[] sack, int S){
        for(int n : nums){
            for(int i = S; i >= n; i--){
                sack[i] += sack[i - n];
            }
        }
    }
}

/*
Time: O(2^n)
public class Solution {
    int count = 0;
    public int findTargetSumWays(int[] nums, int S) {
        helper(nums, S, 0, 0);
        return count;
    }
    private void helper(int[] nums, int S, int eval, int pos){
        if(pos == nums.length){
            if(eval == S){
                count++;
            }
            return;
        }
        helper(nums, S, eval - nums[pos], pos+1);
        helper(nums, S, eval + nums[pos], pos+1);
        
    }
}
*/