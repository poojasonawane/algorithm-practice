/*
Space complexity: O(1)
Time Complexity: O(log n)
*/
public class MissingNumber {
    public int missingNumber(int[] nums) {
        int sum = 0, max = nums[0];
        for(int i = 0; i < nums.length; i++){
            sum += nums[i];
        }
        return (nums.length*(nums.length+1))/2 - sum;
    }
}