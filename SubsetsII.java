/*
Space complexity: O(2 ^ n)
Time Complexity: O(2 ^ n)
*/
public class SubsetsII {
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> res = new ArrayList<List<Integer>>();
        Arrays.sort(nums);
        backtrack(nums, res, new ArrayList<Integer>(), 0);
        return res;
    }
    private void backtrack(int[] nums, List<List<Integer>> res, List<Integer> sub, int start){
        res.add(new ArrayList<Integer>(sub));
        for(int i = start; i < nums.length; i++){
            if(i != start && nums[i] == nums[i-1]) continue;
            sub.add(nums[i]);
            backtrack(nums, res, sub, i+1);
            sub.remove(sub.size()-1);
        }
    }
}