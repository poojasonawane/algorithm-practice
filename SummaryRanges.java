/*
Space complexity: O(n)
Time Complexity: O(n)
*/
import java.util.*;

public class SummaryRanges {
    public List<String> summaryRanges(int[] nums) {
        List<String> result = new ArrayList<String>();
        for(int i = 0; i < nums.length; i++){
            int num = nums[i];
            while(i+1 != nums.length && nums[i]+1 == nums[i+1])
                i++;
            if(num != nums[i])
                result.add(num + "->" + nums[i]);
            else 
                result.add(num + "");
        }
        return result;
    }
}