/*
Space complexity: max(O(m), O(n))
Time Complexity: O(m+n)
*/
import java.util.*;

public class AddTwoNumbers {
    public int c = 0;
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode temp = l1, result = new ListNode(0);
        int c1 = 0, c2 = 0; 
        while(temp != null){
            c1++;
            temp = temp.next;
        }
        temp = l2;
        while(temp != null){
            c2++;
            temp = temp.next;
        }
        result.next = add(l1, l2, c1, c2);
        if(c > 0)
            result.val = c;
        else
            result = result.next;
        return result;
    }
    private ListNode add(ListNode l1, ListNode l2, int c1, int c2){
        ListNode result = new ListNode(0);
        if(c1 == c2){
            if(l1.next != null)
                result.next = add(l1.next, l2.next, c1-1, c2-1);
        }else{
            if(c1 > c2)
                result.next = add(l1.next, l2, c1-1, c2);
            else
                result.next = add(l1, l2.next, c1, c2-1);
        }   
        int sum;
        if(c1 == c2)
            sum = l1.val + l2.val;
        else if(c1 > c2)
            sum = l1.val;
        else
            sum = l2.val;
        sum += c;
        c = sum / 10;
        result.val = sum % 10;
        return result;
    }
}

/*
public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        Stack<Integer> s1 = new Stack<Integer>();
        Stack<Integer> s2 = new Stack<Integer>();
        
        while(l1 != null) {
            s1.push(l1.val);
            l1 = l1.next;
        };
        while(l2 != null) {
            s2.push(l2.val);
            l2 = l2.next;
        }
        
        int sum = 0;
        ListNode list = new ListNode(0);
        while (!s1.empty() || !s2.empty()) {
            if (!s1.empty()) sum += s1.pop();
            if (!s2.empty()) sum += s2.pop();
            list.val = sum % 10;
            ListNode head = new ListNode(sum / 10);
            head.next = list;
            list = head;
            sum /= 10;
        }
        
        return list.val == 0 ? list.next : list;
    }
}
*/