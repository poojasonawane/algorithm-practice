/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

public class RotateFunction {
    public int maxRotateFunction(int[] A) {
        if(A.length == 0)
            return 0;
        int sum = 0, iteration = 0, len = A.length;
        for(int i = 0; i < len; i++){
            sum += A[i];
            iteration += i * A[i];
        }
        int res = iteration;
        for(int i = 1; i < len; i++){
            iteration = iteration - sum + A[i-1] * len;
            res = Math.max(res, iteration);
        }
        return res;
    }
}