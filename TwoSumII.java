/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

public class TwoSumII {
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        int l = 0, h = nums.length - 1;
        while(l < h){
            int sum = nums[l] + nums[h];
            if(sum == target){
                res[0] = l + 1;
                res[1] = h + 1;
                return res;
            }
            if(sum < target)
                l++;
            else
                h--;
        }
        return null;
    }
}