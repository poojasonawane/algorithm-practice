/*
Space complexity: O(n)
Time Complexity: O(1)
*/
import java.util.*;

public class NumArray {
    private int[] nums;
    
    public NumArray(int[] num) {
        nums = new int[num.length];
        if(num.length == 0)
            return;
        nums[0] = num[0];
        for(int i = 1; i < nums.length; i++){
            nums[i] = nums[i-1] + num[i];
        }   
    }

    public int sumRange(int i, int j) {
        if(i == 0)
            return nums[j];
        return nums[j] - nums[i - 1];
    }
}


// Your NumArray object will be instantiated and called as such:
// NumArray numArray = new NumArray(nums);
// numArray.sumRange(0, 1);
// numArray.sumRange(1, 2);