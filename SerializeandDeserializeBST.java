/*
Space complexity: O(w)
Time Complexity: O(n)
*/
public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        Stack<TreeNode> stack = new Stack<TreeNode>();
        StringBuilder result = new StringBuilder();
        if(root == null)
            return null;
        TreeNode curr = root, prev = null;
        while(!stack.isEmpty() || curr != null){
            while(curr != null){
                result.append(curr.val+",");
                stack.push(curr);
                curr = curr.left;
            }
            curr = stack.pop();
            if(curr.right != null && curr.right != prev)
                curr = curr.right;
            else{
                prev = curr;
                //stack.pop();
                curr = null;
            }
        }
        result.deleteCharAt(result.lastIndexOf(","));
        System.out.println(result.toString());
        return result.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        TreeNode root = null;
        if(data == null)
            return null;
        String[] tokens = data.split(",");
        int[] inorder = new int[tokens.length];
        int[] preorder = new int[tokens.length];
        for(int i = 0; i < tokens.length; i++){
            inorder[i] = Integer.parseInt(tokens[i]);
            preorder[i] = inorder[i];
        }
        Arrays.sort(inorder);
        root = buildTree(inorder, 0, inorder.length-1, preorder, 0, preorder.length-1);
        return root;
    }
    
    private TreeNode buildTree(int[] inorder, int inStart, int inEnd, int[] preorder, int preStart, int preEnd){
        if(inStart > inEnd || preStart > preEnd)
            return null;
        int curr = preorder[preStart];
        int offset = inStart;
        while(offset < inEnd){
            if(inorder[offset] == curr)
                break;
            offset++;
        }
        TreeNode node = new TreeNode(curr);
        node.left = buildTree(inorder, inStart, offset-1, preorder, preStart+1, preStart-inStart+offset);
        node.right = buildTree(inorder, offset+1, inEnd, preorder, preStart-inStart+offset+1, preEnd);
        return node;
    }
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.deserialize(codec.serialize(root));