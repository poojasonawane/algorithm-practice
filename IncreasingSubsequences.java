/*
Space complexity: O(2 ^ n)
Time Complexity: O(2 ^ n)
*/
public class IncreasingSubsequences {
    public List<List<Integer>> findSubsequences(int[] nums) {
        Set<List<Integer>> res = new HashSet<List<Integer>>();
        backtrack(res, nums, new ArrayList<Integer>(), 0);
        return new ArrayList(res);
    }
    void backtrack(Set<List<Integer>> res, int[] nums, ArrayList<Integer> sub, int pos){
        if(sub.size() > 1){
                res.add(new ArrayList<Integer>(sub));
        }
        for(int i = pos; i < nums.length; i++){
            if(sub.size() == 0 || sub.get(sub.size() - 1) <= nums[i]){
                sub.add(nums[i]);
                backtrack(res, nums, sub, i+1);
                sub.remove(sub.size()-1);
            }
        }
    }
    
}