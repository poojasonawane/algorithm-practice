/*
Space complexity: O(1)
Time Complexity: O(m+n)
*/
import java.util.*;

public class Search2dMatrixII {
    public boolean searchMatrix(int[][] matrix, int target) {
        if(matrix == null || matrix.length < 1 || matrix[0].length < 1){
            return false;
        }   
        int row = 0;
        int col = matrix[0].length-1;
        while(col >= 0 && row < matrix.length){
            if(matrix[row][col] == target){
                return true;
            }
            if(target < matrix[row][col]){
                col--;
            }else{
                row++;
            }
        }
        return false;
    }
}