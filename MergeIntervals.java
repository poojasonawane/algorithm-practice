/*
Space complexity: O(1)
Time Complexity: O(n log n)
*/
import java.util.*;

/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
public class MergeIntervals {
    public List<Interval> merge(List<Interval> intervals) {
        List<Interval> result = new ArrayList<Interval>();
        Collections.sort(intervals, new Comparator<Interval>(){
            @Override
            public int compare(Interval obj0, Interval obj1) {
                return obj0.start - obj1.start;
            }
        });
        Interval prev = null;
        for(Interval curr : intervals){
            if(prev == null || curr.start > prev.end){
                result.add(curr);
                prev = curr;
            }else {
                prev.end = Math.max(curr.end, prev.end);
            }
        }
        return result;
    }
}