/*
Time Complexity : O(n)
*/
public class Trie {
    private class Node{
        boolean isEnd;
        HashMap<Character, Node> next;
        
        public Node(){
            isEnd = false;
            next = new HashMap();
        }
    }
    
    Node root;
    /** Initialize your data structure here. */
    public Trie() {
        root = new Node();    
    }
    
    /** Inserts a word into the trie. */
    public void insert(String word) {
        Node temp = root;
        char[] wordChar = word.toCharArray();
        for(Character ch : wordChar){
            if(!temp.next.containsKey(ch))
                temp.next.put(ch, new Node());
            temp = temp.next.get(ch);
        }
        temp.isEnd = true;
    }
    
    /** Returns if the word is in the trie. */
    public boolean search(String word) {
        Node node = get(word);
        return node != null && node.isEnd;
    }
    
    /** Returns if there is any word in the trie that starts with the given prefix. */
    public boolean startsWith(String word) {
        Node node = get(word);
        return node != null;
    }
    private Node get(String key) {
        Node node = root;
        for (Character ch : key.toCharArray()) {
            node = node.next.get(ch);
            if (node == null) return null;
        }
        return node;
    }
}

/**
 * Your Trie object will be instantiated and called as such:
 * Trie obj = new Trie();
 * obj.insert(word);
 * boolean param_2 = obj.search(word);
 * boolean param_3 = obj.startsWith(prefix);
 */