/*
Space complexity: O(1)
Time Complexity: O(log (base 5) n)
*/
import java.util.*;

public class TrailingZeroes {
    public int trailingZeroes(int n) {
        int res = 0;
        while(n > 4)
            res += (n /= 5);
        return res;
    }
}