/*
Space complexity: O(1)
Time Complexity: O(n)
*/
import java.util.*;

public class IntToRoman {
   public String intToRoman(int num) {
        int[] values = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
        String[] strs = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
        
        StringBuilder sb = new StringBuilder();
        
        for(int i=0;i<values.length;i++) {
            while(num >= values[i]) {
                num -= values[i];
                sb.append(strs[i]);
            }
        }
        return sb.toString();
    }
}
/*
private static String M[] = {"","M","MM","MMM"};
private static String C[] = {"","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"};
private static String X[] = {"","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"};
private static String I[] = {"","I","II","III","IV","V","VI","VII","VIII","IX"};

public String intToRoman(int num) {
	StringBuilder roman = new StringBuilder();
	return roman.append(M[num/1000]).append(C[(num%1000)/100]).append(X[(num%100)/10]).append(I[num%10]).toString();
}
*/