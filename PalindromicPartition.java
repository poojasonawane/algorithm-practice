/*
Space Complexity: O(n^2)
Time Complexity : O(n^2)
*/
public class PalindromicPartition {
    public List<List<String>> partition(String s) {
        List<List<String>> result = new ArrayList<List<String>>();
        boolean[][] dp = new boolean[s.length()][s.length()];
        for(int i = 0; i < s.length(); i++)
            for(int j = 0; j <= i; j++)
                if(s.charAt(j) == s.charAt(i) && (i - j <= 2 || dp[j+1][i-1]))
                    dp[j][i] = true;
        helper(s, dp, result, new ArrayList<String>(), 0);
        return result;   
    }
    private void helper(String s, boolean[][] dp, List<List<String>> res, List<String> subList, int pos){
        if(pos == s.length()){
            res.add(new ArrayList<String>(subList));
            return;
        }
        for(int i = pos; i < s.length(); i++){
            if(dp[pos][i]){
                subList.add(s.substring(pos, i+1));
                helper(s, dp, res, subList, i+1);
                subList.remove(subList.size()-1);
            }
        }
    }
}

/*
backtracking
public List<List<String>> partition(String s) {
   List<List<String>> list = new ArrayList<>();
   backtrack(list, new ArrayList<>(), s, 0);
   return list;
}

public void backtrack(List<List<String>> list, List<String> tempList, String s, int start){
   if(start == s.length())
      list.add(new ArrayList<>(tempList));
   else{
      for(int i = start; i < s.length(); i++){
         if(isPalindrome(s, start, i)){
            tempList.add(s.substring(start, i + 1));
            backtrack(list, tempList, s, i + 1);
            tempList.remove(tempList.size() - 1);
         }
      }
   }
}

public boolean isPalindrome(String s, int low, int high){
   while(low < high)
      if(s.charAt(low++) != s.charAt(high--)) return false;
   return true;
} 
*/