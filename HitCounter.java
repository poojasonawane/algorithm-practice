/*
Space complexity: O(1)
Time Complexity: O(1), O(s)
*/
public class HitCounter {

    int[] times, hits;
    /** Initialize your data structure here. */
    public HitCounter() {
        times = new int[300];
        hits = new int[300];
    }
    
    /** Record a hit.
        @param timestamp - The current timestamp (in seconds granularity). */
    public void hit(int timestamp) {
        int i = timestamp % 300;
        if(times[i] != timestamp){
            times[i] = timestamp;
            hits[i] = 0;
        }
        hits[i]++;
    }
    
    /** Return the number of hits in the past 5 minutes.
        @param timestamp - The current timestamp (in seconds granularity). */
    public int getHits(int timestamp) {
        int r = 0;
        for(int i = 0; i < 300; i++){
            if(timestamp - times[i] < 300){
                r += hits[i];
            }
        }
        return r;
    }
}

/**
 * Your HitCounter object will be instantiated and called as such:
 * HitCounter obj = new HitCounter();
 * obj.hit(timestamp);
 * int param_2 = obj.getHits(timestamp);
 */