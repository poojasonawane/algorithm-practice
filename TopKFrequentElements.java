/*
Space complexity: O(n)
Time Complexity: O(n)
*/
public class TopKFrequentElements {
    public List<Integer> topKFrequent(int[] nums, int k) {
        HashMap<Integer, Integer> freq = new HashMap<Integer, Integer>();
        for(int num : nums){
            freq.put(num, freq.getOrDefault(num, 0) + 1);
        }
        List<Integer>[] buckets = new List[nums.length+1];
        for(int key : freq.keySet()){
            int idx = freq.get(key);
            List vals;
            if(buckets[idx] == null){
                buckets[idx] = new ArrayList<Integer>();
            }
            buckets[idx].add(key);
        }
        List<Integer> res = new ArrayList<Integer>();
        for(int i = buckets.length-1; i >= 0 && res.size() < k; i--){
            if(buckets[i] != null){
                for(int item : buckets[i]){
                    res.add(item);
                    if(res.size() == k)
                        break;
                }
            }
        }
        return res;
    }
}